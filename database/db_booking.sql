/*
Navicat MySQL Data Transfer

Source Server         : LOKAL_3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_booking

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-10-19 14:14:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for m_kategori
-- ----------------------------
DROP TABLE IF EXISTS `m_kategori`;
CREATE TABLE `m_kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(100) DEFAULT NULL,
  `is_delete` int(11) DEFAULT 0,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of m_kategori
-- ----------------------------
INSERT INTO `m_kategori` VALUES ('1', 'SPARE PART', '0');
INSERT INTO `m_kategori` VALUES ('2', 'AKSESORIS', '0');
INSERT INTO `m_kategori` VALUES ('3', 'JASA', '0');

-- ----------------------------
-- Table structure for m_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `m_pegawai`;
CREATE TABLE `m_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_delete` int(11) DEFAULT 0,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of m_pegawai
-- ----------------------------
INSERT INTO `m_pegawai` VALUES ('1', 'adi', 'adi', 'adi', '0');

-- ----------------------------
-- Table structure for m_pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `m_pelanggan`;
CREATE TABLE `m_pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pelanggan` varchar(100) DEFAULT NULL,
  `alamat_rumah` varchar(100) DEFAULT NULL,
  `alamat_email` varchar(100) DEFAULT NULL,
  `no_hp` varchar(12) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_delete` int(11) DEFAULT 0,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of m_pelanggan
-- ----------------------------
INSERT INTO `m_pelanggan` VALUES ('1', 'CLARA', 'MANAHAN', 'clara@gmail.com', '087823629044', 'CLARA', 'CLARA', '0');
INSERT INTO `m_pelanggan` VALUES ('2', 'ANISA DARAX', 'JL MOJOSONGO NO 19X', 'darax@gmail.com', '087812328920', 'DARAX', 'DARAX', '1');
INSERT INTO `m_pelanggan` VALUES ('3', 'GEA', 'JL MOJOSONGO 19', 'gea@gmail.com', '', 'GEA', 'GEA', '1');
INSERT INTO `m_pelanggan` VALUES ('4', 'GEA', 'JL MOJOSONGO 19', 'gea@gmail.com', '', 'GEA', 'GEA', '1');
INSERT INTO `m_pelanggan` VALUES ('5', 'D', '1', 'd', '1', 'D', 'D', '1');
INSERT INTO `m_pelanggan` VALUES ('6', 'MIRANDA K', 'MANAHAN', 'miranda@gmail.com', '087823629818', 'MIRANDA', 'MIRANDA', '1');

-- ----------------------------
-- Table structure for m_produk
-- ----------------------------
DROP TABLE IF EXISTS `m_produk`;
CREATE TABLE `m_produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produk` varchar(100) DEFAULT '',
  `foto` varchar(100) DEFAULT '',
  `id_kategori` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga_produk` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT 0,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of m_produk
-- ----------------------------
INSERT INTO `m_produk` VALUES ('1', 'KAMPAS GANDA', './assets/images/fotoproduk/KAMPAS GANDA.jpg', '1', '3', '200000', '1');
INSERT INTO `m_produk` VALUES ('2', 'KAMPAS GANDA', './assets/images/fotoproduk/KAMPAS GANDA.jpg', '1', '3', '200000', '1');
INSERT INTO `m_produk` VALUES ('3', 'KAMPAS GANDA', './assets/images/fotoproduk/KAMPAS GANDA.jpg', '1', '3', '300000', '0');
INSERT INTO `m_produk` VALUES ('4', 'Service motor 1000km', '', '3', null, '500000', '0');
INSERT INTO `m_produk` VALUES ('5', 'COVER JOK MOTOR', './assets/images/fotoproduk/COVER JOK MOTOR.jpg', '2', '2', '100000', '0');

-- ----------------------------
-- Table structure for tx_transaksi
-- ----------------------------
DROP TABLE IF EXISTS `tx_transaksi`;
CREATE TABLE `tx_transaksi` (
  `id_trans` int(11) NOT NULL AUTO_INCREMENT,
  `id_produk` int(11) DEFAULT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `tgl_trans` date DEFAULT NULL,
  `qty` int(11) DEFAULT 0,
  `total_harga` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT 0,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tx_transaksi
-- ----------------------------
INSERT INTO `tx_transaksi` VALUES ('1', '4', '1', '2022-10-20', '1', '500000', '0');
INSERT INTO `tx_transaksi` VALUES ('2', '4', '1', '2022-10-20', '1', '500000', '0');
INSERT INTO `tx_transaksi` VALUES ('3', '4', '1', '2022-10-21', '1', '500000', '0');
