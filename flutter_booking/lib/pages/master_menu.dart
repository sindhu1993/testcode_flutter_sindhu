import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'booking_screen.dart';
import 'login_screen.dart';

class MasterMenu extends StatefulWidget {
  @override
  _MasterMenuState createState() => _MasterMenuState();
}

class _MasterMenuState extends State<MasterMenu> {
  int selectIndex = 0;

  late String usernameParam;
  late String id_customer;

  @override
  void initState() {
    super.initState();
    getSharePref();
  }

  getSharePref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      usernameParam = prefs.getString('username').toString();
      id_customer = prefs.getString('id_pelanggan').toString();

      if (usernameParam == "" && id_customer == "") {
        Navigator.of(context).pop();
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => new LoginScreen()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Offstage(
            offstage: selectIndex != 0,
            child: TickerMode(
              enabled: selectIndex == 0,
              child: BookingScreen(),
            ),
          ),
          
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Color.fromRGBO(255, 255, 255, 1),
        // color: Colors.blue,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 1;
                    print(selectIndex);
                  });
                },
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.car_repair,
                      color: (selectIndex == 1)
                          ? Color.fromRGBO(62, 122, 250, 1)
                          : Color.fromRGBO(185, 185, 185, 1),
                    ),
                    Text(
                      'Booking',
                      style: TextStyle(
                          color: (selectIndex == 1)
                              ? Color.fromRGBO(62, 122, 250, 1)
                              : Color.fromRGBO(185, 185, 185, 1)),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
