import 'dart:async';
import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';

import 'package:flutter_booking/pages/master_menu.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import '../network/base_url.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();

  String usernameParam = "";
  String idCustomerParam = "";

  addSharePref(String id_pelanggan, String username) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('id_pelanggan', id_pelanggan);
      prefs.setString('username', username);
    });
  }

  _login() async {
    if (username.text.isEmpty || password.text.isEmpty) {
      ArtSweetAlert.show(
            context: context,
            artDialogArgs: ArtDialogArgs(
              type: ArtSweetAlertType.danger,
              title: "gagal",
              text: "form harus di isi"
            )
      );
    } else {
      print(username.text);
      print(password.text);
      final response = await http.post(Uri.parse(Api.baseUrlLocal + "login"),
          body: {"username": username.text, "password": password.text});
      print(response.body);
      var dataUser = json.decode(response.body);

      if (dataUser.length == 0) {
        setState(() {
          // msg = "Login Fail";
          ArtSweetAlert.show(
            context: context,
            artDialogArgs: ArtDialogArgs(
              type: ArtSweetAlertType.danger,
              title: "gagal",
              text: "gagal login"
            )
          );
        });
      } else {
        setState(() {
          usernameParam = dataUser[0]['username'];
          idCustomerParam = dataUser[0]['id_pelanggan'];
          addSharePref(idCustomerParam, usernameParam);
        });

        Navigator.of(context).pop();
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => new MasterMenu()));
            // return false to keep dialog
            return false;
      }
    }
  }

   _checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      usernameParam = prefs.getString('username').toString();
      idCustomerParam = prefs.getString('id_pelanggan').toString();

      if (idCustomerParam != "") {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => new MasterMenu()));
      }
    });
  }

  
  @override
  void initState() {
    super.initState();
    // _checkLogin();
  }

  @override
  Widget build(BuildContext context) {
     return Scaffold(
      body: SingleChildScrollView(
        // child: _loginWidget(context),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 40.0),
              child: Center(
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: MediaQuery.of(context).size.height * 0.4,
                    child: Text(''),
                ),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: username,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Username',
                    hintText: 'Enter valid username'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: password,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password',
                  ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.035,
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.1,
              width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(30)),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                ),
                onPressed: () {
                  _login();
                },
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}