import 'dart:convert';

import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_booking/network/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class BookingScreen extends StatefulWidget {
  late String id_customer;
  late String username;

  // BookingScreen({this.username, this.id_customer});

  @override
  _BookingScreenState createState() => _BookingScreenState();
}

class _BookingScreenState extends State<BookingScreen> {
  final _formKey = GlobalKey<FormState>();
  late String _valProvince = "";
  List<dynamic> _dataJasa = [];
  void getJasa() async {
    final respose = await http.get(Uri.parse(
        Api.baseUrlLocal + "getJasa")); //untuk melakukan request ke webservice
    var listData = jsonDecode(respose.body); //lalu kita decode hasil datanya
    setState(() {
      _dataJasa = listData; // dan kita set kedalam variable _dataProvince
    });
    // print("data : $listData");
  }

  @override
  void initState() {
    // TODO: implement initState

    getJasa();
    super.initState();
  }

  DateTime date = DateTime.now();
  String month = "";
  String day = "";
  String year = "";

  late TextEditingController _tglTrans = new TextEditingController();
  late TextEditingController _pilKategori = new TextEditingController();
  late TextEditingController _keteranganAlasan = new TextEditingController();

  String inputTglTrans = "";
  String hargaProduk = "";
  String inputPilKategori = "";

  int indexFilter = 0;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(2000),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != date) {
      setState(() {
        date = pickedDate;
        day = date.toString().substring(8, 10);
        month = date.toString().substring(5, 7);
        year = date.toString().substring(0, 4);

        _tglTrans.text = "";
        _tglTrans.text = day + "-" + month + "-" + year;

        inputTglTrans = _tglTrans.text;
        print(_tglTrans.text);
      });
    }

    if (pickedDate == null) {
      inputTglTrans = "";
      _tglTrans.text = "";
      print("cancel button");
    }
  }

  Future<void> _selectKategori(BuildContext context) async {
    return showDialog<void>(
      context: context,
      // barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Pilih Jasa'),
          content: Column(
            children: [
              Flexible(
                child: Container(
                  width: 200,
                  height: MediaQuery.of(context).size.height,
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      // itemCount: 100,
                      itemCount: _dataJasa.length,
                      itemBuilder: (context, index) {
                        // return Text('Kategori ${index}');
                        return InkWell(
                          onTap: () {
                            print(
                                'Kategori ${_dataJasa[index]['id_produk'].toString()}');
                            _pilKategori.text =
                                _dataJasa[index]['nama_produk'].toString();
                            // inputPilKategori = _pilKategori.text;
                            inputPilKategori =
                                _dataJasa[index]['id_produk'].toString();
                            setState(() {
                               hargaProduk = _dataJasa[index]['harga_produk'].toString();
                            });
                           

                            if (inputPilKategori.isNotEmpty) {
                              Navigator.pop(context);
                            }
                          },
                          child: Padding(
                            padding:
                                EdgeInsetsDirectional.fromSTEB(0, 0.5, 0, 0),
                            child: Card(
                              elevation: 2,
                              color: Colors.white.withOpacity(0.9),
                              child: ListTile(
                                  // title: Text('Kategori ${index}'),
                                  title: Text(
                                      '${_dataJasa[index]['nama_produk']}'),
                                  trailing:
                                      (inputPilKategori == index.toString())
                                          ? Icon(Icons.check_box,
                                              color: Colors.green)
                                          : Icon(Icons.check_box,
                                              color: Color(0x00000000))),
                            ),
                          ),
                        );
                      }),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Future<void> _prosesBooking() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    print(inputTglTrans);
    print(inputPilKategori);
    print(prefs.getString('id_pelanggan').toString());

    final response =
        await http.post(Uri.parse(Api.baseUrlLocal + "insert_booking"), body: {
      "id_produk": inputPilKategori,
      "id_pelanggan": prefs.getString('id_pelanggan').toString(),
      "tgl_trans": inputTglTrans,
      "qty":"1",
      "total_harga":hargaProduk.toString()
    });
    print(response.body);
    var dataTrans = json.decode(response.body);
    if (dataTrans.length == 0) {
      setState(() {
        // msg = "Login Fail";
        ArtSweetAlert.show(
            context: context,
            artDialogArgs: ArtDialogArgs(
                type: ArtSweetAlertType.danger,
                title: "gagal",
                text: "gagal booking"));
      });
    } else {
      setState(() {
          // msg = "Login Fail";
          ArtSweetAlert.show(
            context: context,
            artDialogArgs: ArtDialogArgs(
              type: ArtSweetAlertType.success,
              title: "sukses",
              text: "sukses booking"
            )
          );
        });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Booking'),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Text(''),
            ),
            Container(
              width: 200,
              height: 90,
              padding: EdgeInsetsDirectional.fromSTEB(0, 0, 2, 0),
              child: GestureDetector(
                onTap: () {
                  _selectKategori(context);
                  inputPilKategori = _pilKategori.text;
                },
                child: AbsorbPointer(
                  child: Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
                    child: TextField(
                      enabled: true,
                      controller: _pilKategori,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Pilih Jasa",
                      ),
                      onChanged: (String? value) {
                        inputPilKategori = value!;
                      },
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 100,
              height: 50,
              // padding: EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
              child: Text('Harga Produk : ${hargaProduk}'),
            ),
            Container(
              width: 200,
              height: 90,
              padding: EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
              child: GestureDetector(
                onTap: () {
                  _selectDate(context);
                  day = date.toString().substring(8, 10);
                  month = date.toString().substring(5, 7);
                  year = date.toString().substring(0, 4);
                  _tglTrans.text = day + "-" + month + "-" + year;
                  inputTglTrans = _tglTrans.text;
                },
                child: AbsorbPointer(
                  child: Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(10, 0, 0, 0),
                    child: TextField(
                      enabled: true,
                      controller: _tglTrans,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Pilih Tanggal",
                      ),
                      keyboardType: TextInputType.text,
                      onChanged: (String? value) {
                        inputTglTrans = value!;
                      },
                    ),
                  ),
                ),
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  _prosesBooking();
                },
                child: Text('Booking'))
          ],
        ));
  }
}
