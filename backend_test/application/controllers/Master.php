<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Master extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	// Tambahan Sindhu Start

	// kategori Start
	public function list_kategori()
	{
		$this->load->model('model_master');	
		$var['content'] = 'master-kategori';
		$var['js']		= 'js-mkategori';
		$var['ls_kategori']	= $this->model_master->master_kategori();
		$this->load->view('view-index', $var);
	}

	public function cek_kategori()
	{
		$this->load->model('model_master');	
		$result = $this->model_master->master_kategori();
		echo json_encode($result);
	}

	public function load_kategori()
	{
		$this->load->model('model_master');	
		$result = $this->model_master->master_kategori();
		echo json_encode($result);
	}

	public function add_kategori()
	{
		$this->load->model('model_master');	
		$in_nama_kategori = $_POST['in_nama_kategori'];
		// $in_keterangan = $_POST['in_keterangan'];

		// $ck_dup = $this->model_master->cek_kategori($in_nama_kategori);
		// // echo $this->db->last_query();
		// if($ck_dup['total'] > 0){
		// 	echo json_encode(array('status'=>0,'message'=>'Data dengan NIK tersebut sudah ada. Silahkan muat ulang.'));
		// }

		// else
		// {
			$data = array(
				'nama_kategori' => strtoupper($in_nama_kategori),
				// 'keterangan' => strtoupper($in_keterangan),
				// 'insert_by' => $this->session->userdata('id_user')
			);
			$add = $this->db->insert('m_kategori',$data);
	
			if($add)
			{
				echo json_encode(array('status'=>1));
			}
			// var_dump($in_nama_kategori, $in_keterangan);
		// }
	}

	public function edit_kategori()
	{
		$this->load->model('model_master');	

		$edit_nama_kategori = $_POST['edit_nama_kategori'];
		// $edit_keterangan = $_POST['edit_keterangan'];
		$id_kategori = $_POST['id_kategori'];

		// $ck_dup = $this->model_master->cek_kategori($edit_nama_kategori);
		// // echo $this->db->last_query();
		// if($ck_dup['total'] > 0){
		// 	echo json_encode(array('status'=>0,'message'=>'Data dengan NIK tersebut sudah ada. Silahkan muat ulang.'));
		// }

		// else
		// {
			$data = array(
				// 'update_date' => date('Y-m-d H:i:s'),
				'nama_kategori' => strtoupper($edit_nama_kategori),
				// 'keterangan' => strtoupper($edit_keterangan),
				// 'update_by' => $this->session->userdata('id_user')
			);
	
			$this->model_master->update_kategori($data,$id_kategori);
			echo json_encode(array('status'=>1));
		// }
	}

	public function hapus_kategori()
	{
		$this->load->model('model_master');

		$id_kategori = $_POST['id_kategori'];
		$this->model_master->hapus_kategori($id_kategori);
		echo json_encode(array('status'=>1));
	}
	// kategori End

	// Pelanggan Start
	public function list_pelanggan()
	{
		$this->load->model('model_master');	
		$var['content'] = 'master-pelanggan';
		$var['js']		= 'js-mPelanggan';
		$var['ls_pelanggan']	= $this->model_master->master_pelanggan();
		$this->load->view('view-index', $var);
	}

	public function cek_pelanggan()
	{
		$this->load->model('model_master');	
		$result = $this->model_master->master_pelanggan();
		echo json_encode($result);
	}

	public function load_pelanggan()
	{
		$this->load->model('model_master');	
		$result = $this->model_master->master_pelanggan();
		echo json_encode($result);
	}


	public function add_pelanggan()
	{
		$this->load->model('model_master');	
		$in_nama_pelanggan = $_POST['in_nama_pelanggan'];
		$in_email = $_POST['in_email'];
		$in_alamat_rumah = $_POST['in_alamat_rumah'];
		$in_username = $_POST['in_username'];
		$in_password = $_POST['in_password'];
		$in_no_hp = $_POST['in_no_hp'];

		$ck_dup = $this->model_master->cek_pelanggan($in_nama_pelanggan);
		// echo $this->db->last_query();
		if($ck_dup['total'] > 0){
			echo json_encode(array('status'=>0,'message'=>'Data dengan nama tersebut sudah ada. Silahkan muat ulang.'));
		}

		else
		{
			$data = array(
				'nama_pelanggan' => strtoupper($in_nama_pelanggan),
				'alamat_email' => ($in_email),
				'alamat_rumah' => strtoupper($in_alamat_rumah),
				'username' => strtoupper($in_username),
				'password' => strtoupper($in_password),
				'no_hp' => strtoupper($in_no_hp),
				// 'insert_by' => $this->session->userdata('id_user')
			);
			$add = $this->db->insert('m_pelanggan',$data);
	
			if($add)
			{
				echo json_encode(array('status'=>1));
			}
		}
	}

	public function edit_pelanggan()
	{
		$this->load->model('model_master');	

		$edit_nama_pelanggan = $_POST['edit_nama_pelanggan'];
		$edit_email = $_POST['edit_email'];
		$edit_alamat_rumah = $_POST['edit_alamat_rumah'];
		$edit_username = $_POST['edit_username'];
		$edit_password = $_POST['edit_password'];
		$edit_no_hp = $_POST['edit_no_hp'];
		$id_pelanggan = $_POST['id_pelanggan'];

		// $ck_dup = $this->model_master->cek_pelanggan($edit_nama_pelanggan);
		// // echo $this->db->last_query();
		// if($ck_dup['total'] > 0){
		// 	echo json_encode(array('status'=>0,'message'=>'Data dengan NIK tersebut sudah ada. Silahkan muat ulang.'));
		// }

		// else
		// {
			$data = array(
				// 'update_date' => date('Y-m-d H:i:s'),
				'nama_pelanggan' => strtoupper($edit_nama_pelanggan),
				'alamat_email' => ($edit_email),
				'alamat_rumah' => strtoupper($edit_alamat_rumah),
				'username' => strtoupper($edit_username),
				'password' => strtoupper($edit_password),
				'no_hp' => strtoupper($edit_no_hp),
			);
	
			$this->model_master->update_pelanggan($data, $id_pelanggan);
			echo json_encode(array('status'=>1));
		// }
	}

	public function hapus_pelanggan()
	{
		$this->load->model('model_master');

		$id_pelanggan = $_POST['id_pelanggan'];
		$this->model_master->hapus_pelanggan($id_pelanggan);
		echo json_encode(array('status'=>1));
	}
	// Pelanggan End

	// produk
	public function addProduk()
    {
		$this->load->model('model_master');
        $var['content'] = 'view-inproduk';
		$var['js']		= 'js-inproduk';

		$var['ls_kategori'] = $this->model_master->master_kategori();

		// var_dump($var['ls_kota']);
 		$this->load->view('view-index',$var);
	}

	public function proses_add_produk()
	{
		$this->load->model('model_master');
		$data = array_map('strtoupper', $_POST);

		// var_dump($filename = $_FILES['file']['name']);


		$data['foto'] = $_FILES['file']['name'];
		// // $data['insert_by'] = $this->session->userdata('id_user');
		// // $data['is_active'] = 'AKTIF'; //status karyawan active

		// // $check_produk = $this->model_karyawan->cek_karyawan($data['nik'], $data['nama']);
		$check_produk = $this->db->get_where('m_produk',array(
			'nama_produk' => $data['nama_produk'],
			'is_delete' => 0
		));

	
		if($check_produk->num_rows() > 0)
		{
			$check_produk_x = $this->model_master->cek_produk_x($data['nama_produk']);
			echo json_encode(
				array(
					'status'=>9,
					'nama_produk' => $check_produk_x['nama_produk'],
					'message'=>'Data sudah ada.'
				)
			);
		}

		else
		{
			$location = "";
			$default_foto = base_url()."assets/images/images.jpg";
			
			if(empty($_FILES['file']['tmp_name']))
			{
				$data['foto'] = "assets/images/images.jpg";
			}

			else
			{
				$test = explode('.', $_FILES["file"]["name"]);
				$ext = end($test);
				$name = $data['nama_produk'] . '.' . $ext;
		
				$location = './assets/images/fotoproduk/' . $name; 
				$data['foto'] = $location;	
			}

			$fix = (Object)[];
			foreach($data as $key => $value)
			{
				$fix->$key = $value;
			}

			// var_dump($fix);
	
			$insert = $this->db->insert('m_produk',$fix);
	
			if($insert)
			{
				if(empty($_FILES['file']['tmp_name']))
				{
					// move_uploaded_file($default_foto, $location);
				}
				else
				{
					move_uploaded_file($_FILES["file"]["tmp_name"], $location);

					echo json_encode(array('status'=>1,'message'=>'Berhasil Insert Data.'));
				}
				
			}else{
				echo json_encode(array('status'=>1,'message'=>'gagal Insert Data.'));
			}
		}
	}

	public function load_data_produk_datatable()
	{
		$this->load->model('model_datatable_produk');

		$data = [];
		$no = @$_POST['start'];
		$where = "a.is_delete = 0";
		$list = $this->model_datatable_produk->get_datatables($where);

		if($list)
		{
			foreach ($list as $item) {
				// $str_view = base_url('pegawai/view_pegawai/'.$item->id_pegawai);
				// $str_edit = base_url('pegawai/edit_pegawai/'.$item->id_pegawai);
				
				
				$no++;
				$row = [];
				$row[] = $no.".";
				$row[] = $item->nama_produk;
				$row[] = $item->harga_produk;
				$row[] = $item->qty;
				$row[] = $item->nama_kategori;
				// add html for action
				$row[] = "
				<button type='button' class='btn btn-lg' 
				style='padding-top: 0rem; padding-left:0rem;  padding-bottom: 0rem;' 
				onclick='hapusproduk($item->id_produk)'><span style='color: Red;'><i class='fa fa-trash'></i></span></button>";
				
				// $row[] = '<a href="'.site_url('item/edit/'.$item->id_rute).'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Update</a>
				//        <a href="'.site_url('item/del/'.$item->item_id).'" onclick="return confirm(\'Yakin hapus data?\')"  class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>';
				$data[] = $row;
			}
		}
        
        $output = array(
                    "draw" => @$_POST['draw'],
                    "recordsTotal" => $this->model_datatable_produk->count_all($where),
                    "recordsFiltered" => $this->model_datatable_produk->count_filtered($where),
                    "data" => $data,
                );
        // output to json format
        echo json_encode($output);
	}

	public function dataproduk()
	{
		$var['content'] = 'view-listproduk';
		$var['js']		= 'js-listproduk';

		// $var['ls_karyawan'] = $this->model_karyawan->master_karyawan();

		// var_dump($var['ls_kota']);
 		$this->load->view('view-index',$var);
	}

	public function hapusproduk(){
		$id = $_POST['id_produk'];
		$this->load->model('model_master');
        $this->model_master->hapusproduk($id);
		echo json_encode(array('status'=>1));
	}
	// produk

	// transaksi
	public function datatransaksi()
	{
		$this->load->model('model_master');

		$var['content'] = 'view-listtransaksi';
		$var['js']		= 'js-listtransaksi';

		// $var['ls_karyawan'] = $this->model_karyawan->master_karyawan();

		$var['ls_pelanggan'] = $this->model_master->master_pelanggan();
		$var['ls_produk'] = $this->model_master->master_produk();

		// var_dump($var['ls_kota']);
 		$this->load->view('view-index',$var);
	}

	public function load_data_transaksi_datatable()
	{
		$this->load->model('model_datatable_transaksi');

		$data = [];
		$no = @$_POST['start'];
		$where = "a.is_delete = 0";
		$list = $this->model_datatable_transaksi->get_datatables($where);

		if($list)
		{
			foreach ($list as $item) {
				// $str_view = base_url('pegawai/view_pegawai/'.$item->id_pegawai);
				// $str_edit = base_url('pegawai/edit_pegawai/'.$item->id_pegawai);
				
				
				$no++;
				$row = [];
				$row[] = $no.".";
				$row[] = $item->nama_pelanggan;
				$row[] = $item->nama_kategori;
				$row[] = $item->nama_produk;
				$row[] = $item->qty;
				$row[] = $item->total_harga;
				$row[] = $item->tgl_trans;

				// add html for action
				$row[] = "
				<button type='button' class='btn btn-lg' 
				style='padding-top: 0rem; padding-left:0rem;  padding-bottom: 0rem;' 
				onclick='hapusproduk($item->id_trans)'><span style='color: Red;'><i class='fa fa-trash'></i></span></button>";
				
				// $row[] = '<a href="'.site_url('item/edit/'.$item->id_rute).'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Update</a>
				//        <a href="'.site_url('item/del/'.$item->item_id).'" onclick="return confirm(\'Yakin hapus data?\')"  class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>';
				$data[] = $row;
			}
		}
        
        $output = array(
                    "draw" => @$_POST['draw'],
                    "recordsTotal" => $this->model_datatable_transaksi->count_all($where),
                    "recordsFiltered" => $this->model_datatable_transaksi->count_filtered($where),
                    "data" => $data,
                );
        // output to json format
        echo json_encode($output);
	}

	public function addtransaksi()
    {
		$this->load->model('model_master');
        $var['content'] = 'view-intransaksi';
		$var['js']		= 'js-intransaksi';

		$var['ls_pelanggan'] = $this->model_master->master_pelanggan();
		$var['ls_produk'] = $this->model_master->master_produk();

		// var_dump($var['ls_kota']);
 		$this->load->view('view-index',$var);
	}

	public function get_produk_by_id(){
		$id = $_POST['id_produk'];
		$cek = $this->db->query("select * from m_produk where id_produk = $id");

		if($cek->num_rows() > 0){
			$data = $cek->row();
			echo json_encode(array(
				'status' => 1,
				'harga_produk' => $data->harga_produk
			));
		}
	}

	public function insert_transaksi()
	{
		$id_produk = $_POST['id_produk'];
		$id_pelanggan = $_POST['id_pelanggan'];
		$tgl_trans = date('Y-m-d',strtotime($_POST['tgl_trans']));
		$qty = $_POST['qty'];

		$tgl_skr = date('Y-m-d');
		$total_harga = (int)$qty * (int)$_POST['harga_produk'];
		if($tgl_trans > $tgl_skr){
			// $data_prod
			$insert = $this->db->query("insert into tx_transaksi(id_produk, id_pelanggan, tgl_trans, qty, total_harga)
			VALUES('$id_produk','$id_pelanggan','$tgl_trans', '$qty', '$total_harga')");
			if($insert){
				echo json_encode(array(
					'status' => 1,
					'message' => 'sukses insert'
				));
			}else{
				echo json_encode(array(
					'status' => 0,
					'message' => 'gagal insert'
				));
			}
		}else{
			echo json_encode(array(
				'status' => 0,
				'message' => 'tgl harus minimal h+1'
			));
		}
	}

	public function print_transaksi()
	{
		// $this->load->model('model_keuops_rekapgaji');

        $print = $_GET['print'];

        if($print == "true")
        {
            $tgl_awal_x = $_GET['tgl_trans'];

            $tgl_awal = date("Y-m-d", strtotime($tgl_awal_x));

            $data['arr_rekap'] = $this->db->query("select a.nama_produk, x.qty, x.total_harga, j.nama_kategori, x.tgl_trans, p.nama_pelanggan, x.id_trans
			from tx_transaksi as x
			LEFT JOIN m_produk as a ON x.id_produk = a.id_produk
			LEFT JOIN m_kategori as j ON a.id_kategori = j.id_kategori
			LEFT JOIN m_pelanggan as p ON x.id_pelanggan = p.id_pelanggan
			WHERE x.tgl_trans = '$tgl_awal'")->result_array();

			$this->load->view('print_transaksi',$data);

            // ob_start();
                
            //     require_once('./assets/html2pdf/html2pdf.class.php');
            //     $this->load->view('print_transaksi',$data);
            //     $html = ob_get_contents();
            //         ob_end_clean();
            //         try {
            //     $pdf = new HTML2PDF('L', 'F4', 'en', true);
            //     $pdf->pdf->SetDisplayMode('fullpage');
            //     $pdf->WriteHTML($html);
            //     $pdf->Output('Rekap_Laporan_Transaksi.pdf','C');
            //         }catch(Html2PdfException $e){
            //             $html2pdf->clean();

            //     $formatter = new ExceptionFormatter($e);
            //     echo $formatter->getHtmlMessage();

            //     }
        }
	}
	// transaksi
}
