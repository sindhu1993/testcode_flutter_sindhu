<?php

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_login');
        $this->load->model('model_user');
    }

    public function index()
    {
        $var['content'] = 'login-v';
		$var['js']		= 'js-Login';
		$this->load->view('view-login', $var);
    }

    public function proses_login()
    {
        $data = array_map('strtoupper', $_POST);

        // $encrypt_password = md5(sha1($data['password']));
        // $sub_ep = substr($encrypt_password,9,20);

        $check_login = $this->model_login->check_login($data['username'],$data['password']);

        // $count_login = $this->model_login->count_login($data['username']);

        // echo $this->db->last_query();

        $data_update = array(
            'status_login' => 1
        );

        $where = array(
            'username' => $data['username']
        );

        // echo $sub_ep;
        // echo "<br>";
        // echo $data['username'];

        if($check_login['total'] > 0)
        {
			$data_session = array(
				'id_pegawai' => $check_login['id_pegawai'],
				'nama' => $check_login['nama'],
				'username' => $check_login['username'],
				'status' => "islogin",
				);
	
			$this->session->set_userdata($data_session);
	
			echo json_encode(array('status'=>1,'message'=>'Data Ditemukan'));
        }
		
        else
        {
            echo json_encode(array('status'=>9,'message'=>'Data Tidak Ditemukan'));
        }
    }

    public function proses_logout()
    {
        $id_user = $this->session->userdata('id_pegawai');
        // $data_update = array(
        //     'status_login' => 0
        // );

        // $where = array(
        //     'id_uxteser' =>$id_user
        // );

        // $this->db->where($where);
        // $this->db->update('xm_uxteser',$data_update);

        $this->session->sess_destroy();
	    redirect(base_url('login'));
    }

    public function lupa_password()
    {
        $var['content'] = 'lupa-password-v';
		$var['js']		= 'js-LupaPassword';
		$this->load->view('view-login', $var);
    }

    public function proses_lupa_password()
    {
        $data = array_map('strtoupper', $_POST);

        $check_kode = $this->model_login->check_kode($data['generate_kode']);

        // var_dump($check_kode['total']);

        if($check_kode['total']>0)
        {
            echo json_encode(array('status'=>1,'message'=>'Data Ditemukan','generate_kode'=>$check_kode['generate_kode']));
        }
        else
        {
            echo json_encode(array('status'=>0,'message'=>'Data Tidak Ditemukan'));
        }
    }

    public function ubah_password()
    {
        $kode = $this->uri->segment(3);
        $data_user = $this->model_login->check_kode($kode);
        // var_dump($data_user);

        $var['nama_wilayah'] = $data_user['nama_wilayah'];
        $var['content'] = 'ubah-password-v';
		$var['js']		= 'js-UbahPassword';
		$this->load->view('view-login', $var);
    }

    public function proses_ubah_password()
    {
        $data = array_map('strtoupper', $_POST);

        $encrypt_password = md5(sha1($data['password']));
        $sub_ep = substr($encrypt_password,9,20);

       

        $data_update = array(
            'username' => $data['username'],
            'password' => $sub_ep
        );

        // var_dump($data_update);

        $where = array(
            'generate_kode' => $data['generate_kode']
        );

        $up = $this->db->where($where);
        $up = $this->db->update('xm_uxteser',$data_update);

        if($up)
        {
            $data_update_kode = array(
                'generate_kode' => ""
            );
            $this->db->where($where);
            $this->db->update('xm_uxteser',$data_update_kode);
        }

        echo json_encode(array('status'=>1,'message'=>'Data berhasil diupdate'));
    }
}

?>
