<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "islogin"){
			redirect(base_url("login"));
		}
	}
	public function index(){
		$this->load->model('model_master');
		$var['content'] = 'view-home';
		$this->load->view('view-index',$var);
	}

	public function addkaryawan(){
		$var['content'] = 'view-inkaryawan';
		$var['js']		= 'js-inkaryawan';
 		$this->load->view('view-index',$var);
	}

	public function addPinjam(){
		$this->load->model('model_master');
		$this->load->model('model_bpkb');
		$var['data_baris'] = $this->model_master->getNotoken();
		$var['js']	= 'js-inPnjm';
		$var['content'] = 'view-inpnjm';
		$this->load->view('view-index',$var);
	}

	public function listBpkb(){
		$this->load->model('model_master');
		$this->load->model('model_bpkb');
		$var['content'] = 'view-listBpkb';
		$var['js']		= 'js-listbpkb';
		$var['ls_bpkb'] = $this->model_bpkb->getlistBpkb();
		$this->load->view('view-index',$var);
	}
	public function hapusBpkb(){
		$this->load->model('model_bpkb');
		$var = $this->model_bpkb->deleteBpkb($_POST['id']);
		if ($var) {
			echo "1";
		}else{
			echo "0";
		}
	}
	public function getlistIdBpkb(){
		$this->load->model('model_bpkb');
		$var['data'] = $this->model_bpkb->getlistIdbpkb($_POST['id']);
		$this->load->view('detail-bpkb', $var);
	}
	public function geteditIdBpkb(){
		$this->load->model('model_bpkb');
		$this->load->model('model_master');
		$var['data'] = $this->model_bpkb->getlistIdbpkb($_POST['id']);
		$var['ls_box'] = $this->model_master->getlistPenyimpanan();
		$var['ls_merk'] = $this->model_master->getlistMerk();
		$var['ls_warna'] = $this->model_master->getlistWarna();
		$this->load->view('edit-inbpkb', $var);
	}
	public function listApprove(){
		$this->load->model('model_master');
		$this->load->model('model_bpkb');
		$var['content'] = 'view-listBpkbApv';
		$var['js']		= 'js-app';
		$var['ls_bpkb'] = $this->model_bpkb->getlistApprove();
		$this->load->view('view-index',$var);
	}
	public function listPeminjaman(){
		$this->load->model('model_master');
		$var['content'] = 'view-listPinjam';
		$var['js']		= 'js-master';
		$this->load->view('view-index',$var);
	}
	public function listPengangjuanAprove(){
		$this->load->model('model_master');
		$var['content'] = 'view-listbPKB_aprv';
		$var['js']		= 'js-master';
		$this->load->view('view-index', $var);
	}

	public function addKembali(){
		$this->load->model('model_master');
		$this->load->model('model_bpkb');
		$var['data_baris'] = $this->model_master->getNotoken();
		$var['js']	= 'js-inpnjm';
		$var['content'] = 'view-inpngbl';
		$this->load->view('view-index', $var);
	}
}
