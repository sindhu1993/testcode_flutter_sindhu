<?php

header('Access-Control-Allow-Origin: *'); 
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

    public function login()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];

		// $username = $this->uri->segment(3);
		// $password = $this->uri->segment(4);

        // echo json_encode($username);
        // echo "<br>";
        // echo $password;

		$sql = 'SELECT * FROM m_pelanggan WHERE username = "'.$username.'" 
		AND password = "'.$password.'"';

		$data = $this->db->query($sql)->result_array();
		$result = [];
		if(count($data) > 0)
		{
			// echo json_encode($data);

			foreach($data as $k)
			{
				$result[] = array (
					'id_pelanggan' => $k['id_pelanggan'],
					'nama' => $k['nama_pelanggan'],
					'username' => $k['username'],
					'password' => $k['password']
				);
				
			}
			echo json_encode($result);
		} 

		else 
		{
			$result = [];
			echo json_encode($result);
		}
	}

	public function getJasa()
	{
		$sql = 'SELECT * FROM m_produk WHERE id_kategori = 3';

		$data = $this->db->query($sql)->result_array();
		$result = [];
		if(count($data) > 0)
		{
			// echo json_encode($data);

			foreach($data as $k)
			{
				$result[] = array (
					'id_produk' => $k['id_produk'],
					'nama_produk' => $k['nama_produk'],
					'harga_produk' => $k['harga_produk']
				);
				
			}
			echo json_encode($data);
		} 

		else 
		{
			$result = [];
			echo json_encode($result);
		}
	}

	public function insert_booking()
	{
		$id_produk = $_POST['id_produk'];
		$id_pelanggan = $_POST['id_pelanggan'];
		$tgl_trans = date('Y-m-d',strtotime($_POST['tgl_trans']));
		
		$stop_date = date('Y-m-d', strtotime($tgl_trans . ' -1 day'));

		// if($tgl_trans < $stop_date){
		// 	echo json_encode(array(
		// 		'message'=>'Tanggal harus h-1'
		// 	));
		// }

		$qty = $_POST['qty'];
		$total_harga = $_POST['total_harga'];

		$insert = $this->db->query("insert into tx_transaksi(id_produk, id_pelanggan, tgl_trans, qty, total_harga)
		VALUES ('$id_produk', '$id_pelanggan', '$tgl_trans', '$qty', '$total_harga')");

		$result = [];

		if($insert){
			$sql = 'SELECT * FROM tx_transaksi WHERE tgl_trans = "'.$tgl_trans.'" 
			AND id_pelanggan = "'.$id_pelanggan.'" 
			AND id_produk = "'.$id_produk.'"';

			$data = $this->db->query($sql)->result_array();
			if(count($data)> 0){
				foreach($data as $k)
				{
					$result[] = array (
						'id_pelanggan' => $k['id_pelanggan'],
					);
					
				}
				echo json_encode($result);
			}
		}else{
			$result = [];
			echo json_encode($result);
		}
	}


    // public function insert_booking()
    // {
    //     $kode_trans = $_POST['kode_trans'];
    //     $no_mesin = $_POST['no_mesin'];
    //     $tgl_booking = date("Y-m-d", strtotime($_POST['tgl_booking']));
    //     $tipe_servis = $_POST['tipe_servis'];
    //     $keluhan = $_POST['keluhan'];
    //     $id_customer = $_POST['id_customer'];

    //     $status_service = "belum checkin";
    //     $tipe_transaksi = "Mobile";
        
    //     $id_user = 1;

    //     $sql_trans_service = "INSERT INTO trans_service(id_user, id_customer, kode_trans, status_service,
    //     tipe_transaksi) VALUES (
    //         '$id_user','$id_customer', '$kode_trans', '$status_service', '$tipe_transaksi'
    //     )";

    //     $insert_trans = $this->db->query($sql_trans_service);
    //     if($insert_trans)
    //     {
    //         $id_trans = $this->db->insert_id();

    //         $sql_detail_service = "INSERT INTO detail_trans_service(id_trans_service, no_mesin, keluhan,
    //         tipe_servis, tgl_servis) VALUES (
    //             '$id_trans','$no_mesin','$keluhan', '$tipe_servis', '$tgl_booking'
    //         )";

    //         $insert_detail = $this->db->query($sql_detail_service);

    //         if($insert_detail)
    //         {
    //             $sql_list = "SELECT t.kode_trans, t.est_lama_service, t.status_service, t.tipe_transaksi,
    //             dt.keluhan,dt.no_pol, dt.tgl_servis, dt.tipe_servis, dt.no_mesin
    //             FROM trans_service as t
    //             LEFT JOIN detail_trans_service as dt
    //             ON t.id_trans = dt.id_trans_service
    //             LEFT JOIN customer as c
    //             ON c.id_customer = t.id_customer
    //             WHERE t.status_service = 'belum checkin'
    //             AND t.is_delete = 0 AND dt.is_delete = 0
    //             AND tipe_transaksi = 'Mobile'
    //             AND t.id_customer = $id_customer";
    //             $data = $this->db->query($sql_list)->result_array();
    
    //             if(count($data) > 0)
    //             {
    //                 // echo json_encode($data);
    
    //                 foreach($data as $k)
    //                 {
    //                     $result[] = array (
    //                         'kode_trans' => $k['kode_trans'],
    //                         'no_mesin' => $k['no_mesin'],
    //                         'tgl_booking' => $k['tgl_servis'],
    //                         'tipe_servis' => $k['tipe_servis'],
    //                         'keluhan'=>$k['keluhan']
    //                     );
                        
    //                 }
    //                 echo json_encode($result);
    //             } 
    
    //             else 
    //             {
    //                 $result = [];
    //                 // $result = 'kosong';
    //                 echo json_encode($result);
    //             }
    //         }
    //     }
    // }

    //  public function list_history()
    // {
    //     // $sql = "select * from coba";
	// 	$sql = "
	// 	SELECT t.kode_trans, t.est_lama_service, t.status_service, t.tipe_transaksi,
	// 	dt.keluhan,dt.no_pol, dt.tgl_servis, dt.tipe_servis
	// 	FROM trans_service as t
	// 	LEFT JOIN detail_trans_service as dt
	// 	ON t.id_trans = dt.id_trans_service
	// 	LEFT JOIN customer as c
	// 	ON c.id_customer = t.id_customer
	// 	WHERE t.status_service = 'belum checkin'
    //     AND t.is_delete = 0 AND dt.is_delete = 0
    //     AND tipe_transaksi = 'Mobile';
	// 	";
    //     $data = $this->db->query($sql);

    //     $result = [];

    //     if($data->num_rows()>0)
    //     {
    //         foreach($data->result_array() as $k)
    //         {
    //             $result[] = array (
    //                 'kode_trans' => $k['kode_trans'],
    //                 'est_lama_service' => $k['est_lama_service'],
    //                 'status_service' => $k['status_service'],
    //                 'tipe_transaksi' => $k['tipe_transaksi'],
    //                 'keluhan'=>$k['keluhan'],
	// 				'no_pol'=>$k['no_pol'],
	// 				'tgl_servis'=>date("d-m-Y", strtotime($k['tgl_servis'])),
	// 				'tipe_servis' => $k['tipe_servis']
    //             );
                
    //         }
    //         echo json_encode($result);
    //     }

    //     else
    //     {
    //         $result = [];
    //         // $result = 'kosong';
    //         echo json_encode($result);
    //     }
    // }

	// public function list_news()
	// {
	// 	$sql = "select * from berita_mobile where is_delete = 0";
	// 	$data = $this->db->query($sql);
	// 	$result = [];

    //     if($data->num_rows()>0)
    //     {
    //         foreach($data->result_array() as $k)
    //         {
    //             $result[] = array (
    //                 'judul' => $k['judul'],
    //                 'highlights' => $k['highlights'],
    //                 'isi' => $k['isi'],
    //                 'gambar' => $k['gambar'],
    //                 'tgl_post'=> date("d-m-Y", strtotime($k['tgl_post']))
    //             );
                
    //         }
    //         echo json_encode($result);
    //     }

    //     else
    //     {
    //         $result = [];
    //         // $result = 'kosong';
    //         echo json_encode($result);
    //     }		
	// }

    // public function list_news_slider()
	// {
	// 	$sql = "select * from berita_mobile where is_delete = 0 limit 0,5";
	// 	$data = $this->db->query($sql);
	// 	$result = [];

    //     if($data->num_rows()>0)
    //     {
    //         foreach($data->result_array() as $k)
    //         {
    //             $result[] = array (
    //                 'judul' => $k['judul'],
    //                 'highlights' => $k['highlights'],
    //                 'isi' => $k['isi'],
    //                 'gambar' => $k['gambar'],
    //                 'tgl_post'=> date("d-m-Y", strtotime($k['tgl_post']))
    //             );
                
    //         }
    //         echo json_encode($result);
    //     }

    //     else
    //     {
    //         $result = [];
    //         // $result = 'kosong';
    //         echo json_encode($result);
    //     }		
	// }

    // public function list_banner()
	// {
	// 	$sql = "select * from banner where is_delete = 0 limit 0,5";
	// 	$data = $this->db->query($sql);
	// 	$result = [];

    //     if($data->num_rows()>0)
    //     {
    //         foreach($data->result_array() as $k)
    //         {
    //             $result[] = array (
    //                 'judul' => $k['judul'],
    //                 'isi' => $k['isi'],
    //                 'gambar' => $k['gambar'],
    //                 'tgl_post'=> date("d-m-Y", strtotime($k['tgl_post']))
    //             );
                
    //         }
    //         echo json_encode($result);
    //     }

    //     else
    //     {
    //         $result = [];
    //         // $result = 'kosong';
    //         echo json_encode($result);
    //     }		
	// }

    // public function get_kode_trans()
    // {
    //     $sql = "SELECT CONCAT('SRV',max(id_trans)+1) as kode_trans
    //     from trans_service
    //     where is_delete = 0";

    //     $data = $this->db->query($sql);
    //     $result = [];

    //     if($data->num_rows()>0)
    //     {
    //         foreach($data->result_array() as $k)
    //         {
    //             $result[] = array (
    //                 'kode_trans' => $k['kode_trans']
    //             );
                
    //         }
    //         echo json_encode($result);
    //     }

    //     else
    //     {
    //         $result = [];
    //         // $result = 'kosong';
    //         echo json_encode($result);
    //     }		
    // }

    // public function get_customer_by_id()
    // {
    //     // $id_customer = $_POST['id_customer'];
    //     $id_customer = 1;
    //     $sql = "select * from customer where id_customer = $id_customer";
    //     $data = $this->db->query($sql);
    //     $result = [];

    //     if($data->num_rows()>0)
    //     {
    //         foreach($data->result_array() as $k)
    //         {
    //             $result[] = array (
    //                 'nama' => $k['nama'],
    //                 'alamat' => $k['alamat'],
    //                 'no_hp' => $k['no_hp'],
    //                 'email' => $k['email'],
    //                 'username' => $k['username'],
    //                 'password' => $k['password']
    //             );
                
    //         }
    //         echo json_encode($result);
    //     }

    //     else
    //     {
    //         $result = [];
    //         // $result = 'kosong';
    //         echo json_encode($result);
    //     }
    // }

    // public function update_profile()
    // {
    //     $nama = $_POST['nama'];
    //     $no_hp = $_POST['no_hp'];
    //     $email = $_POST['email'];
    //     $username = $_POST['username'];
    //     $alamat = $_POST['alamat'];
    //     $password = $_POST['password'];
    //     $id_customer = $_POST['id_customer'];

    //     $sql_update = "UPDATE customer SET 
    //     nama = '$nama', email = '$email', no_hp = '$no_hp', 
    //     username = '$username', alamat = '$alamat', password = '$password'
    //     where id_customer = $id_customer ";

    //     $u = $this->db->query($sql_update);
    //     if($u)
    //     {
    //         $id_customer = 1;
    //         $sql = "select * from customer where id_customer = $id_customer";
    //         $data = $this->db->query($sql);
    //         $result = [];
    
    //         if($data->num_rows()>0)
    //         {
    //             foreach($data->result_array() as $k)
    //             {
    //                 $result[] = array (
    //                     'nama' => $k['nama'],
    //                     'alamat' => $k['alamat'],
    //                     'no_hp' => $k['no_hp'],
    //                     'email' => $k['email'],
    //                     'username' => $k['username'],
    //                     'password' => $k['password']
    //                 );
                    
    //             }
    //             echo json_encode($result);
    //         }
    
    //         else
    //         {
    //             $result = [];
    //             // $result = 'kosong';
    //             echo json_encode($result);
    //         }
    //     }
    //     else
    //     {
    //         $result = [];
    //         echo json_encode($result);
    //     }
    // }
}
?>
