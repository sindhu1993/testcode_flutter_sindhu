 <section class="content-header">
      
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <!-- Input Absensi Start -->
          <div class="row">
            <div class="col-md-12">
              <div class="card card-info">
                <div class="card-header" style="padding: .4rem 1rem;">
                  <h3 class="card-title">Input Data Absensi</h3>
                </div>
                <form role="form">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Nama</label>
                          <div class="col-sm-8">
                            <!-- <input type="text" class="form-control form-control-sm" id="in_nama" name="in_nama" placeholder="NAMA"> -->
                            <select class="select2 form-control form-control-sm">
                              <option hidden="hidden">NAMA</option>
                              <option>Laki-laki</option>
                              <option>Perempuan</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">NIK</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm " id="in_nik" name="in_nik" placeholder="NIK">
                          </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Tanggal Absen</label>
                          <div class="col-sm-8">
                            <input type="hidden" class="form-control form-control-sm " id="tanggal_absen" name="tanggal_absen" 
                            value="<?php echo date('Y-m-d')?>">

                            <input type="text" readonly="readonly" class="form-control form-control-sm " value="<?php echo date('d-m-Y')?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Status Masuk</label>
                          <div class="col-sm-8">
                            <!-- <input type="text" class="form-control form-control-sm" id="in_status_masuk" name="in_status_masuk" placeholder="STATUS MASUK"> -->
                            <select class="select2 form-control form-control-sm" id="in_status_masuk" name="in_status_masuk">
                                <option disabled="disabled" selected="selected">Pilih Status Masuk</option>
                                <option value="">Cuti</option>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Jam Masuk</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="in_jam_masuk" name="in_jam_masuk" placeholder="JAM MASUK">
                          </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Status Terlambat</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="in_status_terlambat" name="in_status_terlambat" placeholder="STATUS TERLAMBAT">
                          </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Keterangan</label>
                          <div class="col-sm-8">
                            <textarea class="form-control form-control-sm" id="in_keterangan" placeholder="KETERANGAN" name="in_keterangan"></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                      <button type="button" class="btn btn-success btn-sm"
                        style="align-items: right;margin-top:20px;">
                            <i class="fa fa-plus"></i> Simpan
                        </button>
                      </div>
                    </div>
                  </div>
                  </div>
                </form>
                <!-- /.card-body -->
              </div>
            </div>
          </div>
        <!-- Input Absensi End -->

        <!-- Tabel Data Absensi Start -->
          <div class="row">
            <div class="col-md-12">
              <div class="card card-info">
                  <div class="card-header" style="padding: .4rem 1rem;">
                    <h3 class="card-title">Data Input Absensi</h3>
                  </div>
                  <form role="form">
                    <div class="card-body">
                      <div class="row">
                        <div class="table-responsive">
                          <div class="col-md-12">
                              <table id="tabel_absensi" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                  <th>No.</th>
                                  <th>NIK</th>
                                  <th>Nama</th>
                                  <th>Status Masuk</th>
                                  <th>Jam Masuk</th>
                                  <th>Status Terlambat</th>
                                  <th>Keterangan</th>
                                  <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                  <td>1.</td>
                                  <td>Trident</td>
                                  <td>Internet
                                    Explorer 4.0
                                  </td>
                                  <td>Win 95+</td>
                                  <td> 4</td>
                                  <td>X</td>
                                  <td>P</td>
                                  <td>
                                    <button type='button' class='btn btn-warning btn-sm' 
                                    onclick="">Edit</button>
                                    <button type='button' class='btn btn-danger btn-sm' 
                                    onclick=''>Hapus</button>
                                  </td>
                                </tr>
                                </tbody>
                              </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  <!-- /.card-body -->
                </div>
              </div>
              <!-- end -->
          </div>
        <!-- Tabel Data Absensi End -->
      </div>
    </section>