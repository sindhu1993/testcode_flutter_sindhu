<div class="container-fluid">
    <?php
    if($this->session->userdata('level') == "FULL" || $this->session->userdata('level') == "PENGGAJIAN")
    {
        ?>
            <div class="card" style="padding: 5px;">
                <div class="card-header text-white bg-info" >
                    <div class="row">
                    <div class="col-md-10" style="padding-top: 0.5rem;">
                        <h5>Estimasi</h5>
                    </div>
                    <div class="col-md-2">
                        
                    </div>
                    </div>
                
                </div>
                <div class="card-body">
                    
                        <h5 class="card-title">Tabel Data Estimasi</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total AB</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control"  id="total_ab" disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="fname" class="text-right control-label col-form-label">Total Gaji Kotor AB</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="total_gaji_kotor" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label for="fname" class="control-label col-form-label">Transfer Mandiri</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="total_gaji_kotor_mandiri" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label for="fname" class="control-label col-form-label">Transfer BRI</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="total_gaji_kotor_bri" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label for="fname" class="control-label col-form-label">Transfer Tunai</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="total_gaji_kotor_tunai" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <br>
                        <br>
                            <div class="table-responsive">
                                <table id="datatabel_estimasi" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                            <th>No</th>
                                            <th>Nik</th>
                                            <th>Nama</th>
                                            <th>Rekening</th>
                                            <th>Bank</th>
                                            <th>Total Bonus PP (Rp)</th>                    
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    <!-- <tfoot>
                                        <tr>
                                            <th colspan="5" style="text-align:right;font-weight:800;">TOTAL (BERDASARKAN FILTER KOMPONEN) :</th>
                                            <th id="total_nominal" style="font-weight:800;"></th>
                                        </tr>
                                    </tfoot> -->
                                </table>
                            </div>
                    </div>
                </div>

        <?php
    }
    ?>
</div>