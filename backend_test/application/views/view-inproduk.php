 <section class="content-header" style="padding: 5px .1rem;">
      
    </section>
 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">

 <!-- Form Element sizes -->
          
            <div class="card card-info">
              <div class="card-header" style="padding: .4rem 1rem;">
                <h3 class="card-title">Input Produk</h3>
              </div>
              <form role="form" id="contentPegawai" action="">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group row" style="margin-bottom: 0.1rem;">
                        <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Nama</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control form-control-sm" id="nama_produk" col="c_nama_produk"
                          name="nama_produk" placeholder="Nama Produk" style="text-transform: uppercase;">
                        </div>
                      </div>

                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Kategori</label>
                          <div class="col-sm-7">
                          <select class="select2 form-control form-control-sm" name="id_kategori" id="id_kategori" col="c_id_kategori">
                                <option hidden="hidden" value="default">Pilih Kategori</option>
                              <?php 
                              if($ls_kategori)
                              {
                                foreach($ls_kategori as $j)
                                {
                                  ?>
                                  <option value="<?php echo $j['id_kategori'];?>"><?php echo $j['nama_kategori'];?></option>
                                  <?php
                                }
                              }
                              ?>
                            </select>
                          </div>
                        </div>

												<div class="form-group row" style="margin-bottom: 0.1rem;">
													<label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">qty</label>
													<div class="col-sm-8">
														<input type="number" class="form-control form-control-sm" id="qty" col="c_qty"
														name="qty" placeholder="Qty" style="text-transform: uppercase;">
													</div>
												</div>

												<div class="form-group row" style="margin-bottom: 0.1rem;">
													<label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">harga</label>
													<div class="col-sm-8">
														<input type="number" class="form-control form-control-sm" id="harga_produk" col="c_harga_produk"
														name="harga_produk" placeholder="harga" style="text-transform: uppercase;">
													</div>
												</div>

											<div class="form-group row" style="margin-bottom: 0.1rem;">
                        <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Gambar</label>
                        <div class="col-sm-4">
                          <input type="file" data-height="150" name="file" id="file"
                          class="dropify" data-default-file="<?php echo base_url();?>assets/images/images.jpg" />
                          <!-- <input type="file" id="file"> -->
                        </div>
                      </div>

											<div class="form-group row" style="margin-bottom: 0.1rem;">
                        <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm"></label>
                        <div class="col-sm-8">
                        <button type="button" class="btn btn-success btn-md" onclick="submit_produk('tambah')"
                        style="align-items: right;margin-top:20px;">
                            <i class="fa fa-plus"></i> Simpan
                        </button>
                        </div>
                      </div>

                  
                    </div>

                  </div>
                </div>
              </form>
              <!-- /.card-body -->
            </div>
<!-- /.card -->
          </div>
        </div>
      </div>
    </section>
