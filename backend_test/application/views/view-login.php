<!DOCTYPE html>
<html lang="en">
<head>
	<title>Booking Motor</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/logincss/')?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincss/')?>css/main.css">
<!--===============================================================================================-->


<style>
a:hover {
  background-color:blue;
}
</style>

</head>

<body>
	
	<?php $this->load->view($content);?>

	
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincss/')?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincss/')?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincss/')?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('assets/logincss/')?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincss/')?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincss/')?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url('assets/logincss/')?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincss/')?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<!-- <script src="<?php echo base_url('assets/logincss/')?>js/main.js"></script> -->

	<!-- sweet alert cdn -->
    <script src="<?php echo base_url();?>assets/sweetalertbaru/js_sweetalert.js"></script>

	<script type="text/javascript">
    var URL = "<?php echo base_url(); ?>";
    </script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/script/<?php echo $js; ?>.js"></script>

</body>
</html>
