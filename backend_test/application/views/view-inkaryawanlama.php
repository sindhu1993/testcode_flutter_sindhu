<section class="content-header" style="padding: 5px .1rem;">
      
      </section>
   <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
  
   <!-- Form Element sizes -->
            
              <div class="card card-info">
                <div class="card-header" style="padding: .4rem 1rem;">
                  <h3 class="card-title">Input Data Karyawan</h3>
                </div>
                <form role="form" id="contentPegawai" action="">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">NIK</label>
                          <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm" col="c_nik" id="nik" name="nik" placeholder="NIK">
                          </div>
                          <div class="col-sm-4">
                            <button onclick="CEKNik()" type="button" class="btn btn-info btn-sm">
                            <i class="fa fa-search"></i>
                          </button>
                          </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Nama</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="nama" col="c_nama"
                            name="nama" placeholder="Nama Lengkap" style="text-transform: uppercase;">
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Alamat Asal</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Alamat Asal" col="c_alamat_asal"
                            id="alamat_asal" name="alamat_asal" style="text-transform:uppercase;"></textarea>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Alamat Tinggal</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Alamat Tinggal" col="c_alamat_tinggal"
                            id="alamat_tinggal" name="alamat_tinggal" style="text-transform:uppercase;"></textarea>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Tempat, Tgl Lahir</label>
                          <div class="col-sm-6">
                            <select class="select2 form-control form-control-sm" name="tempat_lahir"
                            col="c_tempat_lahir" id="tempat_lahir">
                                <option value="default">Pilih Kota</option>
                              <?php if(!empty($ls_kota)){ ?>
                                  <?php foreach ($ls_kota as $x) { ?>
                                      <option value="<?php echo $x['id_kota']; ?>"><?php echo $x['nama_kota']; ?></option>
                                  <?php } ?>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm"></label>
                          <div class="col-sm-6">
                            <input type="date" class="form-control form-control-sm" 
                            col="c_tgl_lahir" name="tanggal_lahir" placeholder="TANGGAL">
                            <!-- <input type="hidden" name="tgl_lahir_fix" id="inp_tgl_lahir"> -->
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Jenis Kelamin</label>
                          <div class="col-sm-4">
                          <select class="select2 form-control form-control-sm" style="height: calc(1.8125rem + 2px);" 
                          name="jenis_kelamin" col="c_jenis_kelamin"
                          id="jenis_kelamin">
                                <option hidden="hidden" value="default">Pilih Jenis Kelamin</option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Golongan Darah</label>
                          <div class="col-sm-4">
                          <select class="select2 form-control form-control-sm" style="height: calc(1.8125rem + 2px);" 
                          col="c_golongan_darah"
                          name="golongan_darah" id="gol_darah">
                                <option hidden="hidden" value="">Pilih Gol. Darah</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="AB">AB</option>
                                <option value="O">O</option>
                            </select>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">No. Telp</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="no_telp"
                            col="c_no_telp"
                            name="no_telp" placeholder="No. Telp" style="text-transform: uppercase;">
                          </div>
                        </div>
  
                          <div class="form-group row" style="margin-bottom: 0.1rem;">
                            <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Tanggal Masuk</label>
                            <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm"
                            id="tanggal_masuk_x" name="tanggal_masuk" value="<?php echo date('Y-m-d');?>"
                            placeholder="TANGGAL MASUK">
                            </div>
                          </div>
  
                          <div class="form-group row" style="margin-bottom: 0.1rem;">
                            <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Jabatan</label>
                            <div class="col-sm-7">
                            <select class="select2 form-control form-control-sm" name="jabatan_id" id="jabatan" col="c_jabatan">
                                  <option hidden="hidden" value="default">Pilih Jabatan</option>
                                <?php 
                                if($ls_jabatan)
                                {
                                  foreach($ls_jabatan as $j)
                                  {
                                    ?>
                                    <option value="<?php echo $j['id_master_jabatan'];?>"><?php echo $j['nama_jabatan'];?></option>
                                    <?php
                                  }
                                }
                                ?>
                              </select>
                            </div>
                          </div>
  
                          <div class="form-group row" style="margin-bottom: 0.1rem;">
                            <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Status Kawin</label>
                            <div class="col-sm-5">
                            <select class="select2 form-control form-control-sm" name="status_pribadi_id" id="status_kawin" col="status_kawin"
                            onchange="ch_status_pribadi($(this).val());">
                                  <option value="default">Pilih Status</option>
                                  <?php 
                                  if($ls_status_pribadi)
                                  {
                                    foreach($ls_status_pribadi as $x)
                                    {
                                      ?>
                                      <option value="<?php echo $x['id_status_pribadi'];?>"><?php echo $x['nama_status_pribadi'];?></option>
                                      <?php
                                    }
                                  }
                                  ?>
                                
                              </select>
                            </div>
                          </div>
  
                        <div id="show_status_pasangan" style="display: none;">
                          <div class="form-group row" style="margin-bottom: 0.1rem;">
                            <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Nama Istri / Suami</label>
                            <div class="col-sm-8">                        
                              <input type="text" class="form-control form-control-sm " id="nama_pasangan" name="nama_pasangan" value="-"
                              placeholder="Nama Istri atau Suami">
                            </div>
                          </div>
                        </div>
                        <div id="show_status_jml_anak" style="display: none;">
                          <div class="form-group row" style="margin-bottom: 0.1rem;">
                            <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Jumlah Anak</label>
                            <div class="col-sm-8">                        
                              <input type="number" class="form-control form-control-sm "
                              value=0
                              id="jumlah_anak" name="jumlah_anak" placeholder="Jumlah Anak">
                            </div>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Pendidikan</label>
                          <div class="col-sm-4">
                          <select class="select2 form-control form-control-sm" name="pendidikan_id" id="pendidikan" col="c_pendidikan">
                              <option hidden="hidden" value="default">Pilih Pendidikan</option>
                              <?php 
                              if($ls_pendidikan)
                              {
                                foreach($ls_pendidikan as $p)
                                {
                                  ?>
                                  <option value="<?php echo $p['id_pendidikan'];?>"><?php echo $p['nama_pendidikan'];?></option>
                                  <?php
                                }
                              }
                              ?>
                            </select>
                          </div>
                        </div>
  
                    
                      </div>
  
                  <!--kolom Kanan-->
                  
                      <div class="col-md-6">
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Agama</label>
                          <div class="col-sm-4">
                          <select class="select2 form-control form-control-sm" name="agama_id" id="agama" col="c_agama">
                                <option hidden="hidden" value="default">Pilih Agama</option>
                                <?php 
                                if($ls_agama)
                                {
                                  foreach($ls_agama as $a)
                                  {
                                      ?>
                                      <option value="<?php echo $a['id_agama'];?>"><?php echo $a['nama_agama'];?></option>
                                      <?php
                                  }
                                }
                                ?>
                            </select>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">SIM</label>
                          <div class="col-sm-8">                        
                            <input type="text" class="form-control form-control-sm " 
                            id="sim" name="sim" col="c_sim" placeholder="sim" style="text-transform: uppercase;" value="-">
                          </div>
                        </div>
                        
                        <!-- <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Group Karyawan</label>
                          <div class="col-sm-6">
                          <select class="form-control form-control-sm">
                                <option hidden="hidden">Pilih Group Karyawan</option>
                                <option>Laki-laki</option>
                                <option>Perempuan</option>
                            </select>
                          </div>
                        </div> -->
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Wilayah</label>
                          <div class="col-sm-6">
                            <select class="select2 form-control form-control-sm" name="wilayah_id" col="c_wilayah">
                              <option hidden="hidden" value="default">Pilih Wilayah</option>
                              <?php 
                                if($ls_wilayah)
                                {
                                  foreach($ls_wilayah as $a)
                                  {
                                      ?>
                                      <option value="<?php echo $a['id_wilayah'];?>"><?php echo $a['nama_wilayah'];?></option>
                                      <?php
                                  }
                                }
                              ?>
                            </select>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Golongan</label>
                          <div class="col-sm-6">
                            <select class="select2 form-control form-control-sm" name="golongan_id" col="c_golongan">
                              <option hidden="hidden" value="default">Pilih Golongan</option>
                              <?php 
                                if($ls_golongan)
                                {
                                  foreach($ls_golongan as $a)
                                  {
                                      ?>
                                      <option value="<?php echo $a['id_golongan'];?>"><?php echo $a['nama_golongan'];?></option>
                                      <?php
                                  }
                                }
                              ?>
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Keterangan</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Keterangan" name="keterangan" c="c_keterangan" 
                            style="text-transform:uppercase;">-</textarea>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Riwayat Pekerjaan</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" c="c_riwayat_pekerjaan" 
                            name="riwayat_pekerjaan" placeholder="Riwayat Pekerjaan" style="text-transform:uppercase;">-</textarea>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Foto</label>
                          <div class="col-sm-4">
                            <input type="file" data-height="150" name="foto"
                            class="dropify" data-default-file="<?php echo base_url();?>assets/images/images.jpg" />
                            <!-- <input type="file" id="file"> -->
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm"></label>
                          <div class="col-sm-8">
                          <button type="button" class="btn btn-success btn-md" onclick="submit_pegawai('tambah')"
                          style="align-items: right;margin-top:20px;">
                              <i class="fa fa-plus"></i> Simpan
                          </button>
                          </div>
                        </div>
  
                      
                      </div>
                    </div>
                  </div>
                </form>
                <!-- /.card-body -->
              </div>
  <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
  