<section class="content-header" style="padding: 5px .1rem;">      
  <div class="card card-primary card-outline">
      <div class="card-header">
        <div class="row">
          <div class="col-md-8">
              <h3 class="card-title">
                <i class="fas fa-address-card"></i>
                  Data Produk
              </h3>
          </div>
          <div class="col-md-4">
            <button type="button" onclick="addproduk();" class="btn btn-success btn-sm" 
            style="align-items: right;"><i class="fa fa-plus"></i> Add Produk</button>
          </div>
        </div>
      </div>
      <div class="card-body">

      <div id="loading">
        <img src="<?php echo base_url()?>assets/images/loading.gif" width="100px" height="100px">
      </div>

      <br>

      <div class="row mb-4">

        <div class="table-responsive">
          <table id="tbl_karyawan" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>nama produk</th>
              <th>harga</th>
              <th>qty</th>
              <th>kategori</th>
              <th>AKSI</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.card -->
  </div>
</section>
