<section class="content-header">
  <div class="card">
    <div class="card-header" style="background-color: #dadada">
      <div class="row">
        <div class="col-md-9">
          <h3 class="card-title"><i class="fas fa-address-card"></i> Master Pelanggan</h3>
        </div>
        <div class="col-md-3">
          <button type="button" class="btn btn-success btn-sm"
          style="align-items: right;" data-toggle="modal" data-target="#modal_input_pelanggan" data-backdrop="static" data-keyboard="false">
              <i class="fa fa-plus"></i> Add Pelanggan
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
    
      <div id="loading">
          <!-- <img src="<?php echo base_url()?>assets/dist/img/loading.gif" width="100px" height="100px"> -->
      </div>
      
      <div class="table-responsive">
        <div id="show_pelanggan">
        
        </div>
      <div class="table-responsive">
    
    </div>
    <!-- /.card-body -->
  </div>      
</section>

      <!-- modal add start -->
      <div class="modal fade" id="modal_input_pelanggan">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #728ae0;color:white;">
              <h4 class="modal-title">Input Master Pelanggan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form role="form">
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Nama Pelanggan</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm " name="in_nama_pelanggan" id="in_nama_pelanggan" placeholder="Nama Pelanggan">
                    </div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">No HP</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm " id="in_no_hp" placeholder="No Hp">
                    </div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control form-control-sm " id="in_email" placeholder="Email">
                    </div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Alamat Rumah</label>
                    <div class="col-sm-8">
												<textarea class="form-control form-control-sm " id="in_alamat_rumah" placeholder="Alamat Rumah"></textarea>
										</div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Username</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm " id="in_username" placeholder="Username">
                    </div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Password</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm " id="in_password" placeholder="Password">
                    </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" onclick="addPelanggan()">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- modal add end -->

      <!-- modal edit start -->
      <div class="modal fade" id="modal_edit_pelanggan">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #ffc107;color:white;"> 
              <h4 class="modal-title">Edit Master Pelanggan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form role="form">
                <div class="form-group row">

                    <!-- id_agama start -->
                    <input type="hidden" value="" id="id_pelanggan_edit">
                    <!-- end id_agama -->

                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Nama Pelanggan</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " name="edit_nama_pelanggan" id="edit_nama_pelanggan" placeholder="Nama Pelanggan">
                    </div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">No HP</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm " id="edit_no_hp" placeholder="No Hp">
                    </div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control form-control-sm " id="edit_email" placeholder="Email">
                    </div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Alamat Rumah</label>
                    <div class="col-sm-8">
												<textarea class="form-control form-control-sm " id="edit_alamat_rumah" placeholder="Alamat Rumah"></textarea>
										</div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Username</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm " id="edit_username" placeholder="Username">
                    </div>
                </div>
								<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Password</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm " id="edit_password" placeholder="Password">
                    </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" onclick="editPelanggan()">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- modal edit end -->

      <!-- /.modal -->
