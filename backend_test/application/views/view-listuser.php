<section class="content-header" style="padding: 5px .1rem;">      
  <div class="card card-primary card-outline">
      <div class="card-header">
        <div class="row">
          <div class="col-md-9">
              <h3 class="card-title">
                <i class="fas fa-address-card"></i>
                  Data User
              </h3>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-success btn-sm"
            style="align-items: right;" data-toggle="modal" data-target="#modal_input_user" data-backdrop="static" data-keyboard="false">
                <i class="fa fa-plus"></i> Add User
            </button>
          </div>
        </div>
      </div>
      <div class="card-body">

      <div id="loading">
        <img src="<?php echo base_url()?>assets/images/loading.gif" width="100px" height="100px">
      </div>

      <br>

        <div class="table-responsive">
          <div id="user_show">
          
          </div>
        </div>
      </div>
      <!-- /.card -->
  </div>
</section>

<!-- modal add start -->
<div class="modal fade" id="modal_input_user">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #728ae0;color:white;">
              <h4 class="modal-title">Input Data User</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="contentUser" action="">
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">NIK</label>
                    <div class="col-sm-4">
                      <input type="number" class="form-control form-control-sm" col="c_nik" id="in_nik"
                            name="nik" placeholder="NIK" style="text-transform: uppercase;">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Nama</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" col="c_nama" id="in_nama"
                      name="in_nama" placeholder="Nama" style="text-transform: uppercase;">
                    </div>
                </div>
                <div class="form-group row">
                  <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Username</label>
                  <div class="col-sm-4">
                  <input type="text" class="form-control form-control-sm" col="c_username" id="in_username"
                    name="in_username" placeholder="Username" style="text-transform: uppercase;">
                  </div>
                </div>
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Password</label>
                    <div class="col-sm-4">
                      <input type="password" class="form-control form-control-sm" col="c_password" id="in_password"
                      name="in_password" placeholder="Password" style="text-transform: uppercase;">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Level</label>
                    <div class="col-sm-8">
                    <select class="form-control select2bs4" id="in_level" name="in_level" onchange="level_change(this.value)" style="width: 100%;">
                      <?php 
                      if($ls_hak)
                      {
                        foreach($ls_hak  as $x)
                        {
                          ?>
                            <option value="<?php echo $x['id_hak_akses'];?>"><?php echo $x['level'];?></option>
                          <?php
                        }
                      }
                      ?>
                      </select>
                    </div>
                </div>
                <div class="form-group row" id="show_operator" style="display: none">
                  <label for="input-data" class="col-sm-4 col-form-label col-form-label-sm">Operator Wilayah</label>
                  <div class="col-sm-8">
                      <select class="form-control select2bs4" id="in_wilayah" name="in_wilayah" style="width: 100%;">
                      <?php 
                      if($ls_wilayah)
                      {
                        foreach($ls_wilayah  as $x)
                        {
                          ?>
                            <option value="<?php echo $x['id_wilayah'];?>"><?php echo $x['kode_wilayah'];?> - <?php echo $x['nama_wilayah'];?></option>
                          <?php
                        }
                      }
                      ?>
                      </select>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" onclick="submit_user('tambah')">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- modal add end -->

      <!-- modal generate kode start -->
      <div class="modal fade" id="modal_generate_kode">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #ffc107;color:white;"> 
              <h4 class="modal-title">Generate Kode</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form role="form" id="contentUser" action="">
                <div class="form-group row">

                    <!-- id_agama start -->
                    <input type="hidden" value="" id="id_uxteser_g">
                    <!-- end id_agama -->

                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">NIK</label>
                    <div class="col-sm-4">
                    <input type="number" class="form-control form-control-sm" col="c_nik" id="g_nik"
                            name="g_nik" disabled style="text-transform: uppercase;">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Nama</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" col="c_nama" id="g_nama"
                            name="g_nama" disabled style="text-transform: uppercase;">
                    </div>
                </div>
                <div class="form-group row">
                  <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Username</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control form-control-sm" col="c_username" id="g_username"
                    name="g_username" disabled style="text-transform: uppercase;">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Wilayah</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" col="c_password" id="g_wilayah"
                      name="g_wilayah" disabled style="text-transform: uppercase;" value="">
                    </div>
                </div>
                <div class="form-group row">
                  <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Generate Kode</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" col="c_password" id="g_generate_kode"
                      name="generate_kode" disabled style="text-transform: uppercase;">
                    </div>
                    <div class="col-sm-4">
                  <button type="button" class="btn btn-info" onclick="klik_generate()">Klik Generate</button>
                </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" onclick="ubah_kode()">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- modal generate kode end -->

       <!-- modal edit start -->
       <div class="modal fade" id="modal_edit_user">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #ffc107;color:white;"> 
              <h4 class="modal-title">Edit User</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form role="form" id="contentUser" action="">
                <div class="form-group row">

                    <!-- id_agama start -->
                    <input type="hidden" value="" id="id_uxteser_edit">
                    <!-- end id_agama -->

                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">NIK</label>
                    <div class="col-sm-4">
                    <input type="number" class="form-control form-control-sm" col="c_nik" id="edit_nik"
                            name="edit_nik" placeholder="nik" style="text-transform: uppercase;">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Nama</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" col="c_nama" id="edit_nama"
                            name="edit_nama" placeholder="Nama" style="text-transform: uppercase;">
                    </div>
                </div>
                <div class="form-group row">
                <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Username</label>
                <div class="col-sm-4">
                <input type="text" class="form-control form-control-sm" col="c_username" id="edit_username"
                  name="edit_username" placeholder="Username" style="text-transform: uppercase;">
                </div>
              </div>
                <div class="form-group row">
                  <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Password</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" col="c_password" id="edit_password"
                      name="edit_password" placeholder="Password" style="text-transform: uppercase;">
                    </div>
                </div>
                <div class="form-group row">
                  <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Level</label>
                  <div class="col-sm-8">
                    <select class="form-control select2bs4" id="select2_level_edit" name="edit_level" onchange="level_change(this.value)">
                      
                    </select>
                  </div>
                </div>
                <div class="form-group row" id="show_operator_edit" style="display: none">
                  <label for="input-data" class="col-sm-4 col-form-label col-form-label-sm">Operator Wilayah</label>
                  <div class="col-sm-8">
                    <select class="form-control select2bs4" id="edit_wilayah" name="edit_wilayah" style="width: 100%;">
                    <?php 
                      if($ls_wilayah)
                      {
                        foreach($ls_wilayah  as $x)
                        {
                          ?>
                            <option value="<?php echo $x['id_wilayah'];?>"><?php echo $x['kode_wilayah'];?> - <?php echo $x['nama_wilayah'];?></option>
                          <?php
                        }
                      }
                      ?>
                    </select>
                  </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" onclick="submit_user('edit')">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- modal edit end -->