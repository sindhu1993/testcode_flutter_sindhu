<section class="content-header" style="padding: 5px .1rem;">
      
      </section>
   <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
  
   <!-- Form Element sizes -->
            
              <div class="card card-info">
                <div class="card-header" style="padding: .4rem 1rem;">
                  <h3 class="card-title">Input Hak Akses</h3>
                </div>
                <form role="form" id="contentHakAkses" action="">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Level</label>
                          <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm" col="c_level" id="level"
                            name="level" style="text-transform: uppercase;" placeholder="LEVEL">
                          </div>
                        </div>
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">Keterangan</label>
                          <div class="col-sm-8">
                            <textarea name="keterangan" class="form-control form-control-sm"
                            placeholder="Keterangan" id="keterangan" style="text-transform: uppercase;"></textarea>
                          </div>
                        </div>
  
                        <div class="form-group row" style="margin-bottom: 0.1rem;">
                          <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm"></label>
                          <div class="col-sm-8">
                          <button type="button" class="btn btn-success btn-md" onclick="submit_hakakses('tambah')"
                          style="align-items: right;margin-top:20px;">
                              <i class="fa fa-plus"></i> Simpan
                          </button>
                          </div>
                        </div>
  
                    
                      </div>
                    </div>
                  </div>
                </form>
                <!-- /.card-body -->
              </div>
  <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
  