<div class="limiter">
		<div class="container-login100" style="background-color: lightblue;">
			<div class="wrap-login100 p-t-30 p-b-50">
				<form class="login100-form validate-form p-b-33 p-t-5" id="contentLogin">
					<span class="login100-form-title p-b-0" style="color: #000; ">
						Booking Motor
					</span>
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="username" placeholder="Username" col="c_username" 
						style="text-transform:uppercase">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="password" name="password" id="password" placeholder="Password" col="c_password"
						style="text-transform:uppercase">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>

					<p style="margin-left: 70px;">Arahkan Kursor Kesini Untuk Melihat Password.</p>

					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn" type="button" onclick="Login()">
							Login
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	
	<div id="dropDownSelect1"></div>
