<nav class="sidebar-nav">
    <ul id="sidebarnav" class="p-t-30">
      
    <!-- Dashboard Start -->
    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" 
    href="<?php echo base_url()?>home" aria-expanded="false">
      <i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
      <!-- Dashboard End -->

   
	  <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" 
            href="javascript:void(0)" aria-expanded="false"><i class="fas fa-database"></i><span class="hide-menu">Master Data </span></a>
                <ul aria-expanded="false" class="collapse  first-level">
                    <li class="sidebar-item"><a href="<?php echo base_url()?>master/list_kategori" class="sidebar-link"><i class="mdi mdi-brightness-auto"></i>
                    <span class="hide-menu"> Kategori </span></a></li>
					<li class="sidebar-item"><a href="<?php echo base_url()?>master/list_pelanggan" class="sidebar-link"><i class="fa fa-building"></i>
                    <span class="hide-menu"> Pelanggan </span></a></li>
                    <li class="sidebar-item"><a href="<?php echo base_url()?>master/dataproduk" class="sidebar-link"><i class="mdi mdi-school"></i>
                    <span class="hide-menu"> Produk </span></a></li>
                </ul>
        </li>

		<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" 
        href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Transaksi </span></a>
            <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item"><a href="<?php echo base_url('master/datatransaksi')?>" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> List Transaksi Boking</span></a></li>
            </ul>
        </li>
    </ul>
</nav>
