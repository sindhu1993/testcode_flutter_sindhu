  <section class="content-header">  
    <div class="card">
      <div class="card-header" style="background-color: #d8d8d8">
        <div class="row">
          <div class="col-md-9">
            <h3 class="card-title"><i class="fas fa-address-card"></i> Master Kategori</h3>
          </div>
          <div class="col-md-3">
            <button type="button" class="btn btn-success btn-sm"
            style="align-items: right;" data-toggle="modal" data-target="#modal_input_kategori" data-backdrop="static" data-keyboard="false">
                <i class="fa fa-plus"></i> Add Kategori
            </button>
          </div>
        </div>
      </div>
      <div class="card-body">
      
      <div id="loading">
          <img src="<?php echo base_url()?>assets/images/loading.gif" width="100px" height="100px">
      </div>
      
      <div class="table-responsive">
        <div id="kategori_show">
        
        </div>
      </div>

      </div>
      <!-- /.card-body -->
    </div>      
  </section>

      <!-- modal add start -->
      <div class="modal fade" id="modal_input_kategori">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #728ae0;color:white;">
              <h4 class="modal-title">Input Master Kategori</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form role="form">
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Nama Kategori</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " name="in_nama_kategori" id="in_nama_kategori" placeholder="Nama Kategori">
                    </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" onclick="addKategori()">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- modal add end -->

      <!-- modal edit start -->
      <div class="modal fade" id="modal_edit_kategori">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #ffc107;color:white;"> 
              <h4 class="modal-title">Edit Master Kategori</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form role="form">
                <div class="form-group row">

                    <!-- id_kategori start -->
                    <input type="hidden" value="" id="id_kategori_edit">
                    <!-- end id_kategori -->

                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">Nama Kategori</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " name="edit_nama_kategori" id="edit_nama_kategori" placeholder="Nama Kategori">
                    </div>
                </div>
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" onclick="editKategori()">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- modal edit end -->

      <!-- /.modal -->
