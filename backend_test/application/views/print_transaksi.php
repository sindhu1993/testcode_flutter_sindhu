<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Laporan Transaksi</title>

    <style type="text/css">
    .tabel{
        border-collapse: collapse;
    }
    .tabel th, .tabel td{
        padding: 5px;
    }
    </style>
</head>
<body>
    
    <!-- <div style="margin-left: 40px;">
        <img src="<?php echo base_url()?>assets/images/logo_ri.png"
                    style="width:110px;height:80px;">
    </div> -->

    <!-- <div style="margin-top:-70px;margin-left:-50px;">
        <table>
            <tr>
                <td style="height: 40px;">
                </td>
                <th style="font-size: 30px;">Departemen Operasional</th>
            </tr>
            <tr>
                <td style="width: 200px;"></td>
                <th style="font-size: 20px;">PT Rosalia Indah Transport</th>
            </tr>
        </table>
    </div> -->

    <div style="margin-top:-10px;"></div>

    <div style="margin-top: -15px;">
        <table>
            <tr>
                <td align="left"><b><?php echo str_repeat('_',142);?></b></td>
            </tr>
        </table>
    </div>

    <div style="margin-top:-17px;">
        <table>
            <tr>
                <td align="left"><?php echo str_repeat('_',142);?></td>
            </tr>
        </table>
    </div>

    <div style="margin-top: 20px;margin-left:210px;">
        <table>
            <tr>
                <th style="width: 200px;"></th>
                <th style="font-size: 20px;"><u>Laporan Transaksi</u></th>
            </tr>
        </table>
    </div>

    <div style="margin-top: 20px;">
        <table align="center" border="1px" class="tabel" style="font-size:13px;">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>kategori</th>
                <th>produk</th>
                <th>qty</th>
                <th>harga</th>
                <th>tgl trans</th>
            </tr>
            
            <?php 
            $no = 1;
            foreach($arr_rekap as $x)
            {
                ?>
                <tr>
                    <td><?php echo $no;?></td>
                    <td><?php echo $x['nama_pelanggan'];?></td>
                    <td><?php echo $x['nama_kategori'];?></td>
                    <td><?php echo $x['nama_produk'];?></td>
					<td><?php echo $x['qty'];?></td>
                    <td><?php echo "Rp. ".number_format($x['total_harga'], 0,".",".");?></td>
                    <td><?php echo date('d-m-Y',strtotime($x['tgl_trans']));?></td>
                </tr>
                <?php
                $no++;
            }
            ?>
        </table>
    </div>

</body>
</html>
