<section class="content-header" style="padding: 5px .1rem;">      
  <div class="card card-primary card-outline">
      <div class="card-header">
        <div class="row">
          <div class="col-md-8">
              <h3 class="card-title">
                <i class="fas fa-address-card"></i>
                  Data Transaksi
              </h3>
          </div>
          <div class="col-md-4">
            <button type="button" onclick="addtransaksi();" class="btn btn-success btn-sm" 
            style="align-items: right;"><i class="fa fa-plus"></i> Add Transaksi</button>
          </div>
        </div>
      </div>
      <div class="card-body">

      <div id="loading">
        <img src="<?php echo base_url()?>assets/images/loading.gif" width="100px" height="100px">
      </div>

      <br>

	  <div class="row">
		<div class="col-md-6">
			<input type="text" class="form-control form-control-sm" col="c_token"
			id="tgl_trans_filter" name="tgl_trans_filter" placeholder="Tanggal Transaksi" style="text-transform: uppercase;">
			<button type="button" onclick="cetak()">Cetak laporan</button>
		</div>

	  </div>

      <div class="row mb-4">

        <div class="table-responsive">
          <table id="tbl_karyawan" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
			  <th>nama pelanggan</th>
			  <th>nama kategori</th>
              <th>nama produk</th>
              <th>qty</th> 
			  <th>harga</th>
              <th>tgl transaksi</th>
              <th>AKSI</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.card -->
  </div>
</section>

     <!-- modal add start -->
	 <div class="modal fade" id="modal_input_trans">
        <div class="modal-dialog modal-md modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #728ae0;color:white;">
              <h4 class="modal-title">Input Trans</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form role="form">
                <div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">pelanggan</label>
                    <div class="col-sm-4">
					<select class="select2 form-control form-control-sm" name="id_pelanggan" id="id_pelanggan" col="c_id_kategori">
							<option hidden="hidden" value="default">Pilih pelanggan</option>
							<?php 
							if($ls_pelanggan)
							{
							foreach($ls_pelanggan as $j)
							{
								?>
								<option value="<?php echo $j['id_pelanggan'];?>"><?php echo $j['nama_pelanggan'];?></option>
								<?php
							}
							}
							?>
						</select>
						</div>
					</div>
                </div>

				<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">produk</label>
                    <div class="col-sm-4">
					<select class="select2 form-control form-control-sm" name="id_produk" id="id_produk" col="c_id_kategori">
							<option hidden="hidden" value="default">Pilih produk</option>
							<?php 
							if($ls_produk)
							{
							foreach($ls_produk as $j)
							{
								?>
								<option value="<?php echo $j['id_produk'];?>"><?php echo $j['nama_produk'];?></option>
								<?php
							}
							}
							?>
						</select>
						</div>
					</div>
                </div>
				
				<div class="form-group row">
                    <label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">tgl</label>
                    <div class="col-sm-4">
					<input type="text" class="form-control form-control-sm" col="c_token"
            		id="tgl_trans" name="tgl_trans" placeholder="Tanggal Transaksi" style="text-transform: uppercase;">
					</div>
                </div>

              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-success" onclick="addTrans()">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- modal add end -->
