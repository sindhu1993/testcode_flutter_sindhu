<section class="content-header" style="padding: 5px .1rem;">
      
	  </section>
   <!-- Main content -->
	  <section class="content">
		<div class="container-fluid">
		  <div class="row">
			<!-- left column -->
			<div class="col-md-12">
  
   <!-- Form Element sizes -->
			
			  <div class="card card-info">
				<div class="card-header" style="padding: .4rem 1rem;">
				  <h3 class="card-title">Input Transaksi</h3>
				</div>
				<form role="form" id="contentPegawai" action="">
				  <div class="card-body">
					<div class="row">
					  <div class="col-md-12">
						
  
					  <div class="form-group row">
							<label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">pelanggan</label>
							<div class="col-sm-4">
							<select class="select2 form-control form-control-sm" name="id_pelanggan" id="id_pelanggan" col="c_id_kategori">
									<option hidden="hidden" value="default">Pilih pelanggan</option>
									<?php 
									if($ls_pelanggan)
									{
									foreach($ls_pelanggan as $j)
									{
										?>
										<option value="<?php echo $j['id_pelanggan'];?>"><?php echo $j['nama_pelanggan'];?></option>
										<?php
									}
									}
									?>
								</select>
								</div>
							</div>

						<div class="form-group row">
							<label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">produk</label>
							<div class="col-sm-4">
							<select class="select2 form-control form-control-sm" name="id_produk" id="id_produk" col="c_id_kategori">
									<option hidden="hidden" value="default">Pilih produk</option>
									<?php 
									if($ls_produk)
									{
										foreach($ls_produk as $j)
										{
											?>
											<option value="<?php echo $j['id_produk'];?>"><?php echo $j['nama_produk'];?></option>
											<?php
										}
									}
									?>
								</select>
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label for="input-data" class="col-sm-2 col-form-label col-form-label-sm">tgl</label>
							<div class="col-sm-12">
							<input type="text" class="form-control form-control-sm" col="c_token"
							id="tgl_trans" name="tgl_trans" placeholder="Tanggal Transaksi" style="text-transform: uppercase;">
							</div>
						</div>
  
						<div class="form-group row" style="margin-bottom: 0.1rem;">
							<label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">qty</label>
							<div class="col-sm-12">
								<input type="number" class="form-control form-control-sm" id="qty" col="c_qty"
								name="qty" placeholder="Qty" style="text-transform: uppercase;">
							</div>
						</div>

						<div class="form-group row" style="margin-bottom: 0.1rem;">
							<label for="input-data" class="col-sm-3 col-form-label col-form-label-sm">harga</label>
							<div class="col-sm-8">
								<input type="number" class="form-control form-control-sm" id="harga_produk" col="c_harga_produk"
								name="harga_produk" readonly placeholder="harga" style="text-transform: uppercase;">
							</div>
						</div>			  
  
											  <div class="form-group row" style="margin-bottom: 0.1rem;">
						  <label for="input-data" class="col-sm-3 col-form-label col-form-label-sm"></label>
						  <div class="col-sm-8">
						  <button type="button" class="btn btn-success btn-md" onclick="submit_transaksi()"
						  style="align-items: right;margin-top:20px;">
							  <i class="fa fa-plus"></i> Simpan
						  </button>
						  </div>
						</div>
  
					
					  </div>
  
					</div>
				  </div>
				</form>
				<!-- /.card-body -->
			  </div>
  <!-- /.card -->
			</div>
		  </div>
		</div>
	  </section>
  