<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/images/pay.png"> -->
    <title>Booking Motor</title>
    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>assets/libs/flot/css/float-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>assets/dist/css/style.min.css" rel="stylesheet">

    <!-- Datatable -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/extra-libs/multicheck/multicheck.css">
    <link href="<?php echo base_url()?>assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Select2 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/libs/select2/dist/css/select2.min.css">

     <!-- Select2 -->
     <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/select2/select2.css">

    <!-- Date Times Picker -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <!-- Jquery Confrim -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/jquery-confirm/jquery-confirm.min.css" />
    <!-- Anypicker -->
    
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/anypicker/src/anypicker-font.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/anypicker/src/anypicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/anypicker/src/anypicker-ios.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/anypicker/src/anypicker-android.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/anypicker/src/anypicker-windows.css" />

  <!-- Dropify -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dropify/dist/css/dropify.min.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

    <style type="text/css">
    body {
        margin: 0;
        overflow-x: hidden;
        color: #060606;
        background: #fff;
        font-weight: 600;
    }

    #design_user_login{
        color: white;
        font-size: initial;
    }

    .profile-user-img {
        border: 3px solid #adb5bd;
        margin: 0 auto;
        padding: 3px;
        width: 100px;

    }
    .table.table-bordered.dataTable th, table.table-bordered.dataTable td {
        border-left-width: 0;
        padding-bottom: 0.1px;
    }

    .datepicker { 
        z-index: 9999999999999 !important;
        background-color: #ede0ef;
    }
    
   
    </style>
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <a class="navbar-brand" href="<?php echo base_url();?>">
                        <b class="logo-icon p-l-10">
                            <!-- <img src="<?php echo base_url()?>assets/images/pay.png" width="30" alt="homepage" class="light-logo" /> -->
                        </b>
                        <span class="logo-text">
                             <!-- <img src="<?php echo base_url()?>assets/images/payroll.png" alt="homepage" class="light-logo" /> -->
                            
                        </span>
                    </a>
                    
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        
                    
                    </ul>
                    <ul class="navbar-nav float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <span id="design_user_login"><?php echo $this->session->userdata('username');?></span> &nbsp; <img src="<?php echo base_url()?>assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31">
                            </a>
                            
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <!-- <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> Inbox</a> -->
                                <!-- <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                                <div class="dropdown-divider"></div> -->
                                <a class="dropdown-item" href="<?php echo base_url('login/proses_logout')?>"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                <!-- <div class="dropdown-divider"></div>
                                <div class="p-l-30 p-10"><a href="javascript:void(0)" class="btn btn-sm btn-success btn-rounded">View Profile</a></div> -->
                            </div>

                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <?php $this->load->view('view-sidebar')?>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->

             <!-- <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->

            

            <div style="padding: 10px">
            <?php $this->load->view($content);?>
            </div>
            
            
            
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center text-white" style="background-color: #6c757d">
                All Rights Reserved | IT RIT <?php echo date('Y'); ?> | Version : 2.0.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url()?>assets/libs/jquery/dist/jquery.min.js"></script>


    <script src="<?php echo base_url()?>assets/libs/jquery-mask/jquery.mask.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url()?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url()?>assets/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url()?>assets/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url()?>assets/dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="<?php echo base_url()?>assets/dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="<?php echo base_url()?>assets/libs/flot/excanvas.js"></script>
    <script src="<?php echo base_url()?>assets/libs/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/libs/flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url()?>assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url()?>assets/libs/flot/jquery.flot.crosshair.js"></script>



    <script src="<?php echo base_url()?>assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url()?>assets/dist/js/pages/chart/chart-page-init.js"></script>

    <!-- DataTable -->
    <script src="<?php echo base_url();?>assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="<?php echo base_url();?>assets/extra-libs/multicheck/jquery.multicheck.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo base_url();?>assets/libs/select2/dist/js/select2.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url();?>assets/select2/select2.full.js"></script>
    <script src="<?php echo base_url();?>assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    
    <!-- Jquery Confrim -->
    <script src="<?php echo base_url();?>assets/jquery-confirm/jquery-confirm.min.js"></script>

    <!-- sweet alert cdn -->
    <script src="<?php echo base_url();?>assets/sweetalertbaru/js_sweetalert.js"></script>

    <!-- Any picker -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/anypicker/src/anypicker.js"></script>

    <!-- Dropify -->
    <script src="<?php echo base_url();?>assets/dropify/dist/js/dropify.min.js"></script>

    <!-- Jquery UI -->
    <script src="<?php echo base_url()?>assets/libs/jquery-color-master/jquery.color.js"></script>
   
    
    <script type="text/javascript">
    var URL = "<?php echo base_url(); ?>";
    </script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/script/<?php echo $js; ?>.js"></script>

</body>

</html>
