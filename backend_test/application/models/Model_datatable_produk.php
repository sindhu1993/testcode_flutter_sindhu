<?php
class Model_datatable_produk extends CI_Model{

    // start datatables karyawan
		var $column_order = array(null, 'a.nama_produk'); //set column field database for datatable orderable
        var $column_search = array('a.nama_produk'); //set column field database for datatable searchable
        var $order = array('a.id_produk' => 'desc'); // default order
 
        private function _get_datatables($where) {
            $filed = 'a.id_produk, a.nama_produk, a.harga_produk, a.qty, j.nama_kategori
            ';

            $data=	$this->db
                    ->select($filed)
                    ->from('m_produk as a')
                    ->join('m_kategori as j','a.id_kategori = j.id_kategori', 'left')
                    // ->where('a.is_delete',0);
                    ->where($where);
                    // ->where('a.is_active',1);//karyawan aktif;
            $i = 0;
            foreach ($this->column_search as $item) { // loop column
                if(@$_POST['search']['value']) { // if datatable send POST for search
                    if($i===0) { // first loop
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    } else {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }
            
            if(isset($_POST['order'])) { // here order processing
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }  else if(isset($this->order)) {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
 
        function get_datatables($where) {
            $this->_get_datatables($where);
            if(@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
 
        function count_filtered($where) {
            $this->_get_datatables($where);
            $query = $this->db->get();
            return $query->num_rows();
        }
        function count_all() {
            $this->db->from('m_produk');
            return $this->db->count_all_results();
        }
        // end datatables
}
?>
