<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Model_master extends CI_Model {

		
		//  Tambahan Sindhu Start

		//kategori Start
		public function master_kategori()
		{
			$filed = '
					a.id_kategori as id_kategori,
					a.nama_kategori as nama_kategori
					';

			$data=	$this->db
					->select($filed)
					->from('m_kategori as a')
					->where('a.is_delete',0)
					->get();
			if($data->num_rows()>0){
				return $data->result_array();
			}else{
				return null;
			}
		}

		public function cek_kategori($nama_kategori)
		{
			$filed = "count(a.nama_kategori) as total
					";

			$data=	$this->db
					->select($filed)
					->from('m_kategori as a')
					->where('a.is_delete',0)
					->where("a.nama_kategori like '%".$nama_kategori."%'")
					->get();
			if($data->num_rows()>0){
				return $data->row_array();
			}else{
				return null;
			}
		}

		public function update_kategori($data,$id)
		{
			$this->db->where('id_kategori', $id);
		    $this->db->update('m_kategori', $data);
		}

		public function hapus_kategori($id_kategori){
			$now = date('Y-m-d H:i:s');
			$data = array(
				'is_delete' => 1,
				// 'delete_date' => $now,
				// 'delete_by' => $this->session->userdata('id_user')
			);
			$this->db->where('id_kategori',$id_kategori)->update('m_kategori',$data);
		}

		//kategori End 

		//Pelanggan Start
		public function master_pelanggan()
		{
			$filed = 'a.*';

			$data=	$this->db
					->select($filed)
					->from('m_pelanggan as a')
					->where('a.is_delete',0)
					->get();
			if($data->num_rows()>0){
				return $data->result_array();
			}else{
				return null;
			}
		}

		public function cek_pelanggan($nama_pelanggan)
		{
			$filed = "count(a.nama_pelanggan) as total";

			$data=	$this->db
					->select($filed)
					->from('m_pelanggan as a')
					->where('a.is_delete',0)
					->where("a.nama_pelanggan = '".$nama_pelanggan."'")
					->get();
			if($data->num_rows()>0){
				return $data->row_array();
			}else{
				return null;
			}
		}

		public function update_pelanggan($data,$id)
		{
			$this->db->where('id_pelanggan', $id);
		    $this->db->update('m_pelanggan', $data);
		}

		public function hapus_pelanggan($id){
			$now = date('Y-m-d H:i:s');
			$data = array(
				'is_delete' => 1,
				// 'delete_date' => $now,
				// 'delete_by' => $this->session->userdata('id_user')
			);
			$this->db->where('id_pelanggan',$id)->update('m_pelanggan',$data);
		}
		//Pelanggan End

		public function cek_produk_x($nama)
        {
            $filed = 'count(a.nama_produk) as total, a.nama_produk as nama_produk';

			$data=	$this->db
					->select($filed)
                    ->from('m_produk as a')
                    // ->where('a.nik',$nik)
                    //->where("a.nama = '$nama' OR a.nik  = '$nik'")
					->where("a.nama_produk  = '$nama'")
					// ->where('a.is_delete',0)
					->get();
			if($data->num_rows()>0){
				return $data->row_array();
			}else{
				return null;
			}
        }

		public function hapusproduk($id)
        {
            $now = date('Y-m-d H:i:s');
			$data = array(
				'is_delete' => 1,
                // 'delete_date' => $now,
                // 'delete_by' => $this->session->userdata('id_user')
			);
			$this->db->where('id_produk',$id)->update('m_produk',$data);
        }

		public function master_produk()
		{
			$filed = 'a.*';

			$data=	$this->db
					->select($filed)
					->from('m_produk as a')
					->where('a.is_delete',0)
					->get();
			if($data->num_rows()>0){
				return $data->result_array();
			}else{
				return null;
			}
		}
	}
