
$(document).ready(function(){
    // $("#tbl_karyawan").DataTable();
    $('#loading').hide();
    loadproduk('');
});

 //Initialize Select2 Elements
 $('.select2bs4').select2({
   theme: 'classic'
 });

 $('#tgl_trans').datepicker({
	todayHighlight:'TRUE',
	autoclose: true,
	format: 'dd-mm-yyyy',
	showButtonPanel: true
  
  }).change(function() {
	$('#tgl_trans').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  });

  
 $('#tgl_trans_filter').datepicker({
	todayHighlight:'TRUE',
	autoclose: true,
	format: 'dd-mm-yyyy',
	showButtonPanel: true
  
  }).change(function() {
	$('#tgl_trans_filter').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  });

function loadproduk(filter_is_active = '')
{
    $('#loading').show();
    $('#loading').hide();
    $('#tbl_karyawan').DataTable({
        "processing":true,
        "serverSide":true,
        "ajax":{
            "url":URL+"master/load_data_transaksi_datatable",
            "type":"POST",
          "data":{
            filter_is_active : filter_is_active
          }
        }
    });
}

function addtransaksi()
{
	 window.location.href = URL+"master/addtransaksi";
	// $('#modal_input_trans').modal('show');
}

function hapusproduk(id)
{
  Swal.fire({
		title: 'Apakah Anda yakin Menghapus Data ini ?',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, Hapus Data'
	}).then((result) => {
		if (result.value) {

				$.ajax({
						'url':URL+'master/hapusproduk',
						'type':'POST',
						'data':{
								id_produk:id
						},
						success:function(res){
								var r = JSON.parse(res);
								if(r.status == 1)
								{
										//success show success modal
										
											Swal.fire(
												'Terhapus!',
												'Data Telah Dihapus',
												'success'
										).then((result) => {
											if(result.value)
											{
												window.location.href = URL+'master/dataproduk';
											}
										});   
								}
								else
								{
									Swal.fire({
											icon: 'error',
											title: 'Perhatian...',
											text: 'Data Gagal Dihapus',
									});
								}
						}
				});
		}
});
}


function cetak()
{
  var tgl_trans_filter = $('#tgl_trans_filter').val();

  if(tgl_trans_filter == "")
  {
	  Swal.fire({
				  icon: 'warning',
				  title: 'Perhatian...',
				  text: 'Masukkan Filter Tanggal',
			  });
  }
  else
  {
	window.open(URL+'master/print_transaksi?print=true&tgl_trans='+tgl_trans_filter,'_blank');
  }
}
