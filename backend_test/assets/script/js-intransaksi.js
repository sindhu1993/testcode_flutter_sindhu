$(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    
     // Basic
     $('.dropify').dropify({
      messages: {
          default: 'Pilih Foto untuk Di Upload',
          replace: 'Silahkan Pilih Foto',
          remove:  'Hapus Foto'
      }
    });

   });
   
   const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar:true
  });

   window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');

  $('#tgl_trans').datepicker({
	todayHighlight:'TRUE',
	autoclose: true,
	format: 'dd-mm-yyyy',
	showButtonPanel: true
  
  }).change(function() {
	$('#tgl_trans').datepicker({ dateFormat: 'dd-mm-yy' }).val();
  });


  $('#id_produk').on('change',function(){
	var id_produk = $('#id_produk').val();
	$.ajax({
		'url':URL+'master/get_produk_by_id',
		'type':'POST',
		'data':{
			id_produk:id_produk
		},
		success:function(data){
			var res = JSON.parse(data);
			if(res.status == 1){
				$('#harga_produk').val(res.harga_produk)
			}
		}
	});
  });

  function submit_transaksi()
  {
	var id_pelanggan = $('#id_pelanggan').val();
	var id_produk = $('#id_produk').val();
	var tgl_trans = $('#tgl_trans').val();
	var qty = $('#qty').val();
	var harga_produk = $('#harga_produk').val();

	$.ajax({
		'url':URL+'master/insert_transaksi',
		'type':'POST',
		'data':{
			id_produk:id_produk,
			id_pelanggan:id_pelanggan,
			tgl_trans:tgl_trans,
			qty:qty,
			harga_produk:harga_produk
		},
		success:function(data){
			var res = JSON.parse(data);
			if(res.status == 1){
				Swal.fire(
					'sukses!',
					''+res.message,
					'success'
				).then((result) => {
					if(result.value)
					{
						window.location.href = URL+'master/datatransaksi';
					}
				});   
			}else{
				if(res.status == 0){
					Swal.fire({
						icon: 'error',
						title: 'Perhatian...',
						text: ''+res.message,
					});
				}
			}
		}
	})
  }

  
