
$(document).ready(function(){
    // $("#tbl_karyawan").DataTable();
    $('#loading').hide();
    loadproduk('');
});

 //Initialize Select2 Elements
 $('.select2bs4').select2({
   theme: 'classic'
 })

function loadproduk(filter_is_active = '')
{
    $('#loading').show();
    $('#loading').hide();
    $('#tbl_karyawan').DataTable({
        "processing":true,
        "serverSide":true,
        "ajax":{
            "url":URL+"master/load_data_produk_datatable",
            "type":"POST",
          "data":{
            filter_is_active : filter_is_active
          }
        }
    });
}


function addproduk()
{
  window.location.href = URL+"master/addProduk";
}


function hapusproduk(id)
{
  Swal.fire({
		title: 'Apakah Anda yakin Menghapus Data ini ?',
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, Hapus Data'
	}).then((result) => {
		if (result.value) {

				$.ajax({
						'url':URL+'master/hapusproduk',
						'type':'POST',
						'data':{
								id_produk:id
						},
						success:function(res){
								var r = JSON.parse(res);
								if(r.status == 1)
								{
										//success show success modal
										
											Swal.fire(
												'Terhapus!',
												'Data Telah Dihapus',
												'success'
										).then((result) => {
											if(result.value)
											{
												window.location.href = URL+'master/dataproduk';
											}
										});   
								}
								else
								{
									Swal.fire({
											icon: 'error',
											title: 'Perhatian...',
											text: 'Data Gagal Dihapus',
									});
								}
						}
				});
		}
});
}
