
$(document).ready(function(){
  loadUser();
    $("#tabel_master_user").DataTable();
    $('#loading').hide();
});

 //Initialize Select2 Elements
 $('.select2bs4').select2();

 function loadUser()
 {
  $('#loading').show();
  $.ajax({
      'url':URL+'user/load_user',
      'type':'POST',
      'async' : false,
      'dataType' : 'json',
      success:function(data){
          var html = "";
          var nama_wilayah_get = "";
          $('#loading').hide();
          if(data)
          {
              html += '<table id="tabel_master_user" class="table table-bordered table-striped">'+
              '<thead>'+
                  '<tr>'+
                      '<th>No</th>'+
                      '<th>NIK</th>'+
                      '<th>Nama</th>'+
                      '<th>Level</th>'+
                      '<th>Nama Wilayah</th>'+
                      '<th>Aksi</th>'+
                  '</tr>'+
              '</thead>'+
            '<tbody id="agama_ajax">';
              for(var i=0; i < data.length; i++)
              {
                if(data[i].nama_wilayah == null)
                {
                  nama_wilayah_get = "0";
                }
                else
                {
                  nama_wilayah_get = data[i].nama_wilayah;
                }
                  html +="<tr>"+
                      "<td>"+(i+1)+"</td>"+
                      "<td>"+data[i].nik+"</td>"+
                      "<td>"+data[i].nama+"</td>"+
                      "<td>"+data[i].keterangan+"</td>"+
                      "<td>"+nama_wilayah_get+"</td>"+
                      "<td><button type='button' class='btn btn-lg' style='padding-top: 0rem; padding-left:0rem; padding-bottom: 0rem;' onclick='show_modal_edit("+ data[i].nik + ",\""+data[i].nama+"\",\""+data[i].username+"\",\""+data[i].password+"\","+data[i].id_hak_akses+","+data[i].id_uxteser+","+data[i].id_wilayah+")'><span style='color: Green;'><i class='fa fa-edit'></i></span></button>"+
                      "&nbsp &nbsp <button type='button' class='btn btn-lg' style='padding-top: 0rem; padding-left:0rem;  padding-bottom: 0rem;' onclick='hapususer("+ data[i].nik + ",\""+data[i].nama+"\","+data[i].id_hak_akses+","+data[i].id_uxteser+")'><span style='color: Red;'><i class='fa fa-trash'></i></span></button>"+
                      "&nbsp &nbsp <button type='button' class='btn btn-lg' style='padding-top: 0rem; padding-left:0rem; padding-bottom: 0rem;' onclick='show_modal_generate_kode("+data[i].id_uxteser+")'><span style='color: Blue;'><i class='fa fa-key'></i></span></button>"+
                      "</td>"+
                  "</tr>";
              }
              html +='</tbody></table>';
              $('#user_show').html(html);
          }

          else
          {
              html += '<table id="tabel_master_agama" class="table table-bordered table-striped">'+
              '<thead>'+
                  '<tr>'+
                    '<th>No</th>'+
                    '<th>NIK</th>'+
                    '<th>Nama</th>'+
                    '<th>Level</th>'+
                    '<th>Nama Wilayah</th>'+
                    '<th>Aksi</th>'+
                  '</tr>'+
              '</thead>'+
            '<tbody id="agama_ajax">';
              html +="<tr>"+
                      "<td valign='top' colspan='6' class='dataTables_empty'>No data available in table</td>"+
                  "</tr>";
              html +='</tbody></table>';
              $('#user_show').html(html);
          }
      }
  });
 }

function level_change(id_hak_akses)
{
  $.ajax({
    'url':URL+'hakakses/check_level',
    'type':'POST',
    'data':{
      id_hak_akses:id_hak_akses
    },
    success:function(data){
      var r = JSON.parse(data);
      if(r.status == 1 || r.status == "1")
      {
        $('#show_operator').show();
        $('#show_operator_edit').show();
      }
      if(r.status == 0 || r.status == "0")
      {
        $('#show_operator').hide();
        $('#show_operator_edit').hide();
      }
    }
  });
}

function load_level(p_id_level)
{
  // alert(p_id_level);
  var html = "";
  $.ajax({
      'url':URL+'hakakses/load_hak_akses',
      'type':'POST',
      'async' : false,
      'dataType' : 'json',
      success:function(data){
          if(data)
          {
              for(var i=0; i < data.length; i++)
              {
                  if(p_id_level == data[i].id_hak_akses)
                  {
                      html += '<option value='+data[i].id_hak_akses+' selected>'+data[i].level+' - '+data[i].keterangan+'</option>';
                  }
                  else
                  {
                      html += '<option value='+data[i].id_hak_akses+'>'+data[i].level+' - '+data[i].keterangan+'</option>';
                  }
              }
              $('#select2_level_edit').html(html);
          }
      }
  });
}

function show_modal_edit(p_nik, p_nama, p_username, p_password, id_hak_akses, p_id_uxteser, p_id_wilayah)
{
  load_level(id_hak_akses);
  if(p_id_wilayah == null)
  {
    $('#show_operator_edit').hide();
  }
  else
  {
    $('#show_operator_edit').show();
  }

  $('#edit_nik').val(p_nik);
  $('#edit_nama').val(p_nama);
  $('#edit_username').val(p_username);
  $('#edit_password').val(p_password);

  $('#id_uxteser_edit').val(p_id_uxteser);
  $('#modal_edit_user').modal('show');
}

function show_modal_generate_kode(p_id_uxteser)
{
  $.ajax({
    'url':URL+'user/cek_user_by_id',
    'type':'POST',
    'data':{
      id_uxteser:p_id_uxteser
    },
    success:function(data){
      var res = JSON.parse(data);

      $('#g_nik').val(res.nik);
      $('#g_nama').val(res.nama);
      $('#g_username').val(res.username);
      $('#g_wilayah').val(res.nama_wilayah);
      $('#g_level').val(res.level);
    
      $('#id_uxteser_g').val(res.id_uxteser);
    }
  });
  $('#modal_generate_kode').modal('show');
}

function klik_generate()
{
  $.ajax({
    'url':URL+'user/generate_kode',
    'type':'POST',
    'data':{
      'action':'generate'
    },
    success:function(data){
      var r = JSON.parse(data);

      if(r.status == 1 || r.status == "1")
      {
        $('#g_generate_kode').val(r.generate_kode);
      }
    }
  });
}

function ubah_kode()
{
  var id_uxteser = $('#id_uxteser_g').val();
  var generate_kode = $('#g_generate_kode').val();

  $.ajax({
    'url':URL+'user/simpan_kode',
    'type':'POST',
    'data':{
      'id_uxteser':id_uxteser,
      'generate_kode':generate_kode
    },
    success:function(data){
      var r = JSON.parse(data);

      if(r.status == 1 || r.status == "1")
      {
        Swal.fire({
          icon: 'success',
          title: 'Perhatian...',
          text: 'Kode Berhasil Di Update',
        });
        $('#modal_generate_kode').modal('hide');
        loadUser();
        $("#tabel_master_user").DataTable();
      }
    }
  });
}

function submit_user(x)
{
  var in_nik = $('#in_nik').val();
  var in_nama = $('#in_nama').val();
  var in_username = $('#in_username').val();
  var in_password = $('#in_password').val();
  var in_level = $('#in_level').val();
  var in_wilayah = $('#in_wilayah').val(); 

  var edit_nik = $('#edit_nik').val();
  var edit_nama = $('#edit_nama').val();
  var edit_username = $('#edit_username').val();
  var edit_password = $('#edit_password').val();
  var edit_level = $('#edit_level').val();
  var edit_wilayah = $('#edit_wilayah').val(); 
  var id_uxteser_edit = $('#id_uxteser_edit').val();

  if(x == 'tambah')
  {
    if(in_nik == "" || in_nama == "" || in_password == "" || in_username == "")
    {
      Swal.fire({
        icon: 'error',
        title: 'Perhatian...',
        text: 'Inputan Wajib Diisi',
      });
    }
    else
    {
      $.ajax({
        'url':URL+'user/insert_user',
        'type':'POST',
        'data':{
          'nik' : in_nik,
          'nama' :in_nama,
          'username':in_username,
          'password':in_password,
          'level':in_level,
          'wilayah':in_wilayah,
        },
        success:function(data){
          var r = JSON.parse(data);
          if(r.status == 1 || r.status== "1")
          {
            $('#in_nik').val("");
            $('#in_nama').val("");
            $('#in_username').val("");
            $('#in_password').val("");

            $('#modal_input_user').modal('hide');
            Swal.fire({
                icon: 'success',
                title: 'Perhatian...',
                text: 'Data Berhasil Ditambahkan',
            });
            loadUser();
            $("#tabel_master_user").DataTable();
          }

          if(r.status == 9 || r.status == "9")
          {
            Swal.fire({
              icon: 'warning',
              title: 'Perhatian...',
              text: 'Data dengan NIK : '+r.nik+ ' dan Nama : '+r.nama+' sudah ada',
          });
          }
        }
       });
    }
  }

  if(x == 'edit')
  {
    if(edit_nik == "" || edit_nama == "" || edit_password == "" || edit_username == "")
    {
      Swal.fire({
        icon: 'error',
        title: 'Perhatian...',
        text: 'Inputan Wajib Diisi',
      });
    }
    else
    {
      $.ajax({
        'url':URL+'user/proses_edit',
        'type':'POST',
        'data':{
          'nik' : edit_nik,
          'nama' :edit_nama,
          'username':edit_username,
          'password':edit_password,
          'level':edit_level,
          'wilayah':edit_wilayah,
          'id_uxteser':id_uxteser_edit
        },
        success:function(data){
          var r = JSON.parse(data);
          if(r.status == 1 || r.status== "1")
          {
            $('#edit_nik').val("");
            $('#edit_nama').val("");
            $('#edit_password').val("");
            $('#edit_username').val("");

            $('#modal_edit_user').modal('hide');
            Swal.fire({
                icon: 'success',
                title: 'Perhatian...',
                text: 'Data Berhasil Diupdate',
            });
            loadUser();
            $("#tabel_master_user").DataTable();
          }

          if(r.status == 9 || r.status == "9")
          {
            Swal.fire({
              icon: 'warning',
              title: 'Perhatian...',
              text: 'Data dengan NIK : '+r.nik+ ' dan Nama : '+r.nama+' sudah ada',
          });
          }
        }
       });
    }
  }
}


function hapususer(nik,nama,id_tx_hak_akses, id)
{
    Swal.fire({
        title: 'Apakah Anda yakin Menghapus User Nama : '+nama+'?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus Data'
      }).then((result) => {
        if (result.value) {

            $.ajax({

                'url':URL+'user/hapususer',
                'type':'POST',
                'data':{
                    id_uxteser:id
                },
                success:function(res){
                    var r = JSON.parse(res);
                    if(r.status == 1)
                    {
                        //success show success modal
                        
                          Swal.fire(
                            'Terhapus!',
                            'Data Telah Dihapus',
                            'success'
                        ).then((result) => {
                          if(result.value)
                          {
                            window.location.href = URL+'user/dataUser';
                          }
                        });   
                    }
                    else
                    {
                      Swal.fire({
                          icon: 'error',
                          title: 'Perhatian...',
                          text: 'Data Gagal Dihapus',
                      });
                    }
                }
            });
        }
    });
}

  