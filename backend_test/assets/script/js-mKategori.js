$(document).ready(function(){
    loadKategori();
    $("#tabel_master_kategori").DataTable();
    cek_kategori();
    $('#loading').hide();
});

var arr_nama_kategori = [];

function cek_kategori()
{
    $.ajax({
        'url':URL+'master/cek_kategori',
        'type':'POST',
        'dataType':'JSON',
        success:function(data){
            $('#loading').hide();
            for(var i=0; i < data.length; i++)
            {
                arr_nama_Kategori.push(data[i].nama_kategori);
            }
        }
    });
}

function loadKategori()
{
    $('#loading').show();
    $.ajax({
        'url':URL+'master/load_kategori',
        'type':'POST',
        'async' : false,
        'dataType' : 'json',
        success:function(data){
            var html = "";
            $('#loading').hide();
            if(data)
            {
                html += '<table id="tabel_master_kategori" class="table table-bordered table-striped">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Nama Kategori</th>'+
                        '<th>Aksi</th>'+
                    '</tr>'+
                '</thead>'+
              '<tbody id="kategori_ajax">';
                for(var i=0; i < data.length; i++)
                {
                    html +="<tr>"+
                        "<td>"+(i+1)+"</td>"+
                        "<td>"+data[i].nama_kategori+"</td>"+
                        "<td><button type='button' class='btn btn-lg' style='padding-top: 0rem; padding-left:0rem; padding-bottom: 0rem;' onclick='show_modal_edit(\""+ data[i].nama_kategori + "\","+data[i].id_kategori+")'><span style='color: Green;'><i class='fa fa-edit'></i></span></button>"+
                        "&nbsp &nbsp <button type='button' class='btn btn-lg' style='padding-top: 0rem; padding-left:0rem;  padding-bottom: 0rem;' onclick='hapusKategori(\""+ data[i].nama_kategori + "\","+data[i].id_kategori+")'><span style='color: Red;'><i class='fa fa-trash'></i></span></button>"+
                        "</td>"+
                    "</tr>";
                }
                html +='</tbody></table>';
                $('#kategori_show').html(html);
            }

            else
            {
                html += '<table id="tabel_master_kategori" class="table table-bordered table-striped">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Nama Kategori</th>'+
                        '<th>Aksi</th>'+
                    '</tr>'+
                '</thead>'+
              '<tbody id="kategori_ajax">';
                html +="<tr>"+
                        "<td valign='top' colspan='3' class='dataTables_empty'>No data available in table</td>"+
                    "</tr>";
                html +='</tbody></table>';
                $('#kategori_show').html(html);
            }
        }
    });
}

function addKategori()
{
    var in_nama_kategori = $('#in_nama_kategori').val().toUpperCase();

    if(in_nama_kategori=="")
    {
        Swal.fire({
            icon: 'error',
            title: 'Perhatian...',
            text: 'Inputan Wajib Diisi',
        });
    }

    else
    {
        // if(arr_nama_kategori.includes(in_nama_kategori))
        // {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Perhatian...',
        //         text: 'Data Sudah Ada',
        //     });
        // }

        // else
        // {
            $.ajax({
                'url':URL+'master/add_kategori',
                'type':'POST',
                'data':{
                    in_nama_kategori:in_nama_kategori
                },
                success:function(res){
                    var r = JSON.parse(res);
                    if(r.status == 1 || r.status == "1")
                    {
                        //success show success modal
                        
                            $('#in_nama_kategori').val("");
        
                            $('#modal_input_kategori').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Perhatian...',
                                text: 'Data Berhasil Ditambahkan',
                            });
                        
                        arr_nama_kategori = [];
                        cek_kategori();
                        loadKategori();
                        $("#tabel_master_kategori").DataTable();
                    }

                    else
                    {
                        if(r.status == 0)
                        {
                            Swal.fire({
                                icon: 'error',
                                title: 'Perhatian...',
                                text: 'Data Sudah Ada',
                            });
                        }
                        else
                        {
                            Swal.fire({
                                icon: 'error',
                                title: 'Perhatian...',
                                text: 'Data Gagal Ditambahkan',
                            });
                        }
                    }
                }
            });
        // }
        
    }
}

function show_modal_edit(p_nama, p_id_kategori)
{
    $('#id_kategori_edit').val(p_id_kategori);
    $('#edit_nama_kategori').val(p_nama);
    $('#modal_edit_kategori').modal('show');
}

function editKategori()
{
    var id_kategori = $('#id_kategori_edit').val();
    var edit_nama_kategori = $('#edit_nama_kategori').val().toUpperCase();

    // if(arr_nama_kategori.includes(edit_nama_kategori))
    // {
    //     Swal.fire({
    //         icon: 'error',
    //         title: 'Perhatian...',
    //         text: 'Data Sudah Ada',
    //     });
    // }

    // else
    // {
        $.ajax({
            'url':URL+'master/edit_kategori',
            'type':'POST',
            'data':{
                edit_nama_kategori:edit_nama_kategori,
                id_kategori:id_kategori
            },
            success:function(res){
                var r = JSON.parse(res);
                if(r.status == 1 || r.status == "1")
                {
                    //success show success modal
                    
                    $('#modal_edit_kategori').modal('hide');
                    Swal.fire({
                        icon: 'success',
                        title: 'SUCCESS...',
                        text: 'Data Berhasil Diperbaharui',
                    });
                    
                    arr_nama_kategori = [];
                    cek_kategori();
                    loadKategori();
                    $("#tabel_master_kategori").DataTable();      
                }
                else
                {
                    if(r.status == 0)
                    {
                        Swal.fire({
                            icon: 'error',
                            title: 'Perhatian...',
                            text: 'Data Sudah Ada',
                        });
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'error',
                            title: 'Perhatian...',
                            text: 'Data Gagal Diperbaharui',
                        });
                    }
                }
            }
        });   
    // }
}

function hapusKategori(p_nama, p_id_kategori)
{
    var id_kategori_hapus = p_id_kategori;

    Swal.fire({
        title: 'Apakah Anda yakin Menghapus Data Kategori : '+p_nama+'?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus Data'
      }).then((result) => {
        if (result.value) {

            $.ajax({

                'url':URL+'master/hapus_kategori',
                'type':'POST',
                'data':{
                    id_kategori:id_kategori_hapus
                },
                success:function(res){
                    var r = JSON.parse(res);
                    if(r.status == 1)
                    {
                        //success show success modal
                        
                        $('#modal_edit_kategori').modal('hide');
                          Swal.fire(
                            'Terhapus!',
                            'Data Telah Dihapus',
                            'success'
                        );
                        
                        arr_nama_kategori = [];
                        cek_kategori();
                        loadKategori();  
                        $("#tabel_master_kategori").DataTable();    
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'error',
                            title: 'Perhatian...',
                            text: 'Data Gagal Dihapus',
                        });
                    }
                }
            });
        }
    });
}


