




function hapusPegawai(id, nama)
{

    Swal.fire({
        title: 'Apakah Anda yakin Menghapus Data Pegawai : '+nama+'?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus Data'
      }).then((result) => {
        if (result.value) {

            $.ajax({

                'url':URL+'pegawai/hapusPegawai',
                'type':'POST',
                'data':{
                    id_pegawai:id
                },
                success:function(res){
                    var r = JSON.parse(res);
                    if(r.status == 1)
                    {
                        //success show success modal
                        
                          Swal.fire(
                            'Terhapus!',
                            'Data Telah Dihapus',
                            'success'
                        ).then((result) => {
                          if(result.value)
                          {
                            window.location.href = URL+'pegawai/dataKaryawan';
                          }
                        });   
                    }
                    else
                    {
                      Swal.fire({
                          icon: 'error',
                          title: 'Perhatian...',
                          text: 'Data Gagal Dihapus',
                      });
                    }
                }
            });
        }
    });
}

function UpdateKeterangan()
{
  var keterangan = $('textarea[name=keterangan]').val();
  var id_master_pegawai = $('input[name=id_master_pegawai]').val();

  // alert(riwayat_pekerjaan+" "+id_master_pegawai);

  $.ajax({
    'url':URL+'pegawai/update_keterangan',
    'type':'POST',
    'data':{
      id_master_pegawai :id_master_pegawai,
      keterangan:keterangan
    },
    success:function(data){
      var r = JSON.parse(data);

      if(r.status == 1)
      {
        $('textarea[name=keterangan]').text(r.keterangan);
        Swal.fire({
          icon: 'success',
          title: 'Perhatian...',
          text: 'Data Berhasil Diperbaharui',
        }).then((result) => {
          if (result.value) {
            window.location.reload();
          }
        });
      }
      else
      {
        Swal.fire({
          icon: 'error',
          title: 'Perhatian...',
          text: 'Data Gagal Dihapus',
        });
      }
    }
  });
}

function UpdateRiwayatPekerjaan()
{
  var riwayat_pekerjaan = $('textarea[name=riwayat_pekerjaan]').val();
  var id_master_pegawai = $('input[name=id_master_pegawai]').val();

  // alert(riwayat_pekerjaan+" "+id_master_pegawai);

  $.ajax({
    'url':URL+'pegawai/update_riwayat_pekerjaan',
    'type':'POST',
    'data':{
      id_master_pegawai :id_master_pegawai,
      riwayat_pekerjaan:riwayat_pekerjaan
    },
    success:function(data){
      var r = JSON.parse(data);

      if(r.status == 1)
      {
        $('textarea[name=riwayat_pekerjaan]').text(r.riwayat_pekerjaan);
        Swal.fire({
          icon: 'success',
          title: 'Perhatian...',
          text: 'Data Berhasil Diperbaharui',
        }).then((result) => {
          if (result.value) {
            window.location.reload();
          }
        });;
      }
      else
      {
        Swal.fire({
          icon: 'error',
          title: 'Perhatian...',
          text: 'Data Gagal Dihapus',
        });
      }
    }
  });
}