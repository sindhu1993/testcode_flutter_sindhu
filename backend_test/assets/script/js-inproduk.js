  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    
     // Basic
     $('.dropify').dropify({
      messages: {
          default: 'Pilih Foto untuk Di Upload',
          replace: 'Silahkan Pilih Foto',
          remove:  'Hapus Foto'
      }
    });

   });
   
   const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar:true
  });

   window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');

  function submit_produk(x)
  {
    if(x == 'tambah')
    {
			var fd = new FormData();
			var nama_produk = $('#nama_produk').val();
			var id_kategori = $('#id_kategori').val();
			var qty = $('#qty').val();
			var harga_produk = $('#harga_produk').val();

			var foto = $('#file')[0].files;

			if(nama_produk == "" || id_kategori == "default" || qty == "" || harga_produk == "" || foto.length < 0){
				Swal.fire({
					icon: 'error',
					title: 'Perhatian...',
					text: 'Form belum terisi',
				});
			}else{
				fd.append('nama_produk',nama_produk);
				fd.append('id_kategori',id_kategori);
				fd.append('qty',qty);
				fd.append('harga_produk',harga_produk);
				fd.append('file',foto[0]);

				$.ajax({
					url:URL+'master/proses_add_produk',
					type:'POST',
					data: fd,
					contentType: false,
					processData: false,
					success: function(response){
						var res = JSON.parse(response);
						 if(res == 0){
							Swal.fire({
								icon: 'error',
								title: 'Perhatian...',
								text: 'gagal insert data',
							});
						 }else{
							Swal.fire({
								icon: 'success',
								title: 'sukses...',
								text: 'insert data sukses',
							});
						 }
					},
			 });
			}
    }
}
