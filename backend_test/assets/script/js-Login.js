
 const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar:true
});


function Login()
{
    var arrInput = $('#contentLogin').find('input[name]').toArray();
    var value_input = "";

    var username = ''

    arrInput.forEach(function(item,index){
        value_input = $('#contentLogin').find('input[name]').eq(index).val();
        if(value_input == "")
        {
          $('#contentLogin').parent().find('input[col]').eq(index).animate({'backgroundColor':'red'},200).animate({'backgroundColor':'white'},200);
          Toast.fire({
            icon: 'warning',
            title: 'Inputan Harus Diisi'
          });
        }
    });

    if(value_input !== "")
    {
        //start ajax
        var form = document.querySelector('form');
  
        var request = new XMLHttpRequest();
        var formData = new FormData(form);
    
        form.addEventListener('submit',function(e){
          e.preventDefault();
        },false);
    
        request.onreadystatechange = function(){
          if(request.readyState == 4 && request.status == 200)
          {
            var json = JSON.parse(request.responseText);
            if(json.status == 1)
            {
              // alert('sukses');
              window.location.href = URL+'home/index';
            }

            else
            {
              if(json.status == 9)
              {
                Swal.fire({
                  icon: 'warning',
                  title: 'Perhatian...',
                  text: 'Username atau Password Tidak Ditemukan',
                });
              }
              else
              {
                if(json.status == 8)
                {
                  Swal.fire({
                    icon: 'warning',
                    title: 'WARNING',
                    text: 'Username Sudah Digunakan.',
                  });
                }
              }
            }
          }
        }
        request.open('post',URL+'login/proses_login',true);
        request.send(formData);
    }
}

$(document).ready(function(){
  $("p").hover(function(){
  //   $(this).css("background-color", "yellow");
    $('#password').attr('type', 'text'); 
    }, function(){
  //   $(this).css("background-color", "pink");
    $('#password').attr('type', 'password'); 
  });
});