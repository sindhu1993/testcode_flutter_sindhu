$(document).ready(function(){
    loadPelanggan();
    $("#tabel_master_pelanggan").DataTable();
    cek_pelanggan();
});

var arr_nama_pelanggan = [];

function cek_pelanggan()
{
    $.ajax({
        'url':URL+'master/cek_pelanggan',
        'type':'POST',
        'dataType':'JSON',
        success:function(data){
            $('#loading').hide();
            for(var i=0; i < data.length; i++)
            {
                arr_nama_pelanggan.push(data[i].nama_pelanggan);
            }
        }
    });
}

function loadPelanggan()
{
    
    $('#loading').show();
    $.ajax({
        'url':URL+'master/load_pelanggan',
        'type':'POST',
        'async' : false,
        'dataType' : 'json',
        success:function(data){
            var html = "";
            $('#loading').hide();
            if(data)
            {
                html += '<table id="tabel_master_pelanggan" class="table table-bordered table-striped">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Nama Pelanggan</th>'+
                        '<th>Alamat Rumah</th>'+
						'<th>Alamat Email</th>'+
						'<th>No Hp</th>'+
                        '<th>Aksi</th>'+
                    '</tr>'+
                '</thead>'+
              '<tbody id="pelanggan_ajax">';
                for(var i=0; i < data.length; i++)
                {
                    html +="<tr>"+
                        "<td>"+(i+1)+"</td>"+
                        "<td>"+data[i].nama_pelanggan+"</td>"+
						"<td>"+data[i].alamat_rumah+"</td>"+
                        "<td>"+data[i].alamat_email+"</td>"+
						"<td>"+data[i].no_hp+"</td>"+
                        "<td><button type='button' class='btn btn-lg' style='padding-top: 0rem; padding-left:0rem; padding-bottom: 0rem;' onclick='show_modal_edit(\""+ data[i].nama_pelanggan + "\", \""+data[i].alamat_rumah+"\", \""+data[i].alamat_email+"\", \""+data[i].no_hp+"\", \""+data[i].username+"\", \""+data[i].password+"\", "+data[i].id_pelanggan+")'><span style='color: Green;'><i class='fa fa-edit'></i></span></button>"+
                        "&nbsp &nbsp <button type='button' class='btn btn-lg' style='padding-top: 0rem; padding-left:0rem;  padding-bottom: 0rem;' onclick='hapusPelanggan(\""+ data[i].nama_pelanggan + "\","+data[i].id_pelanggan+")'><span style='color: Red;'><i class='fa fa-trash'></i></span></button>"+
                        "</td>"+
                    "</tr>";
                }
                html +='</tbody></table>';
                $('#show_pelanggan').html(html);
            }

            else
            {
                html += '<table id="tabel_master_pelanggan" class="table table-bordered table-striped">'+
                '<thead>'+
                    '<tr>'+
                        '<th>No</th>'+
                        '<th>Nama Pelanggan</th>'+
                        '<th>Alamat Rumah</th>'+
						'<th>Alamat Email</th>'+
						'<th>No HP</th>'+
                        '<th>Aksi</th>'+
                    '</tr>'+
                '</thead>'+
              '<tbody id="pelanggan_ajax">';
                html +="<tr>"+
                        "<td valign='top' colspan='6' class='dataTables_empty'>No data available in table</td>"+
                    "</tr>";
                html +='</tbody></table>';
                $('#show_pelanggan').html(html);
            }
        }
    });
}

function addPelanggan()
{
    var in_nama_pelanggan = $('#in_nama_pelanggan').val().toUpperCase();
    var in_email = $('#in_email').val();
	var in_alamat_rumah = $('#in_alamat_rumah').val().toUpperCase();
    var in_username = $('#in_username').val();
	var in_password = $('#in_password').val();
	var in_no_hp = $('#in_no_hp').val();

    if(in_nama_pelanggan == "" || in_email == "" || in_alamat_rumah == "" || in_username == "" || in_password == "" || in_no_hp == "")
    {
        Swal.fire({
            icon: 'error',
            title: 'Perhatian...',
            text: 'Inputan Wajib Diisi',
        });
    }

    else
    {
        // if(arr_nama_pelanggan.includes(in_nama_pelanggan))
        // {
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Perhatian...',
        //         text: 'Data Sudah Ada',
        //     });
        // }

        // else
        // {
            $.ajax({
                'url':URL+'master/add_pelanggan',
                'type':'POST',
                'data':{
                    in_nama_pelanggan:in_nama_pelanggan,
                    in_email:in_email,
					in_alamat_rumah:in_alamat_rumah,
					in_username:in_username,
					in_password:in_password,
					in_no_hp:in_no_hp
                },
                success:function(res){
                    var r = JSON.parse(res);
                    if(r.status == 1)
                    {
                        //success show success modal
                        
                            $('#in_nama_pelanggan').val("");
                            $('#in_email').val("");
							$('#in_alamat_rumah').val("");
							$('#in_username').val("");
							$('#in_password').val("");
							$('#in_no_hp').val("");
        
                            $('#modal_input_pelanggan').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'SUCCESS...',
                                text: 'Data Berhasil Ditambahkan',
                            });
                        
                        arr_nama_pelanggan = [];
                        // arr_keterangan = [];
                        cek_pelanggan();
                        loadPelanggan();
                        $("#tabel_master_pelanggan").DataTable();
                            
                    }
                    else
                    {
                        if(r.status == 0)
                        {
                            Swal.fire({
                                icon: 'error',
                                title: 'Perhatian...',
                                text: 'Data Sudah Ada',
                            });
                        }
                        else
                        {
                            Swal.fire({
                                icon: 'error',
                                title: 'Perhatian...',
                                text: 'Data Gagal Ditambahkan',
                            });
                        }
                    }
                }
            });
        // }
        
    }
}

// \""+ data[i].nama_pelanggan + "\", \""+data[i].alamat_rumah+"\", \""+data[i].alamat_email+"\", \""+data[i].no_hp+"\", 
// \""+data[i].username+"\", \""+data[i].password+"\", "+data[i].id_pelanggan+"

function show_modal_edit(p_nama, p_alamat_rumah, p_alamat_email, p_no_hp, p_username, p_password, p_id_pelanggan)
{
    $('#id_pelanggan_edit').val(p_id_pelanggan);
    $('#edit_nama_pelanggan').val(p_nama);
    $('#edit_email').val(p_alamat_email);

	$('#edit_alamat_rumah').val(p_alamat_rumah);
    $('#edit_username').val(p_username);
	$('#edit_password').val(p_password);

	$('#edit_no_hp').val(p_no_hp);
    $('#modal_edit_pelanggan').modal('show');
}

function editPelanggan()
{
    var id_pelanggan = $('#id_pelanggan_edit').val();
    var edit_nama_pelanggan = $('#edit_nama_pelanggan').val().toUpperCase();
    var edit_nama_pelanggan = $('#edit_nama_pelanggan').val().toUpperCase();
    var edit_email = $('#edit_email').val();
	var edit_alamat_rumah = $('#edit_alamat_rumah').val().toUpperCase();
    var edit_username = $('#edit_username').val();
	var edit_password = $('#edit_password').val();
	var edit_no_hp = $('#edit_no_hp').val();


    // if(arr_nama_pelanggan.includes(edit_nama_pelanggan))
    // {
    //     Swal.fire({
    //         icon: 'error',
    //         title: 'Perhatian...',
    //         text: 'Data Sudah Ada',
    //     });
    // }

    // else
    // {
        $.ajax({
            'url':URL+'master/edit_pelanggan',
            'type':'POST',
            'data':{
                edit_nama_pelanggan:edit_nama_pelanggan,
				edit_email:edit_email,
				edit_alamat_rumah:edit_alamat_rumah,
				edit_username:edit_username,
				edit_password:edit_password,
				edit_no_hp:edit_no_hp,
                id_pelanggan:id_pelanggan
            },
            success:function(res){
                var r = JSON.parse(res);
                if(r.status == 1)
                {
                    //success show success modal
                    
                    $('#modal_edit_pelanggan').modal('hide');
                    Swal.fire({
                        icon: 'success',
                        title: 'SUCCESS...',
                        text: 'Data Berhasil Diperbaharui',
                    });
                    
                    arr_nama_pelanggan = [];
                    arr_keterangan = [];
                    cek_pelanggan();
                    loadPelanggan();
                    $("#tabel_master_pelanggan").DataTable();      
                }
                else
                {
                    if(r.status == 0)
                    {
                        Swal.fire({
                            icon: 'error',
                            title: 'Perhatian...',
                            text: 'Data Sudah Ada',
                        });
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'error',
                            title: 'Perhatian...',
                            text: 'Data Gagal Diperbaharui',
                        });
                    }
                }
            }
        });
    // }
}

function hapusPelanggan(p_nama, p_id_pelanggan)
{
    var id_pelanggan = p_id_pelanggan;

    Swal.fire({
        title: 'Apakah Anda yakin Menghapus Data Pelanggan : '+p_nama+'?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus Data'
      }).then((result) => {
        if (result.value) {

            $.ajax({

                'url':URL+'master/hapus_pelanggan',
                'type':'POST',
                'data':{
                    id_pelanggan:id_pelanggan
                },
                success:function(res){
                    var r = JSON.parse(res);
                    if(r.status == 1)
                    {
                        //success show success modal
                        
                        $('#modal_edit_pelanggan').modal('hide');
                          Swal.fire(
                            'Terhapus!',
                            'Data Telah Dihapus',
                            'success'
                        );
                        
                        arr_nama_pelanggan = [];
                        arr_keterangan = [];
                        cek_pelanggan();
                        loadPelanggan();  
                        $("#tabel_master_pelanggan").DataTable();    
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'error',
                            title: 'Perhatian...',
                            text: 'Data Gagal Dihapus',
                        });
                    }
                }
            });
        }
    });
}
